package hibernate;

import org.hibernate.Session;
import javax.persistence.EntityManager;
import java.io.Serializable;
import java.util.List;

public class GeneralManager {

    private static EntityManager em;

    public static void initHibernate() {
        em = HibernateUtils.getEntityManager();
    }

    public static void saveRow(Serializable serializable) {
        em.getTransaction().begin();
        em.persist(serializable);
        em.getTransaction().commit();
    }

    public static void saveOrUpdate(Serializable serializable) {
        em.getTransaction().begin();
        Session s = em.unwrap(Session.class);
        s.saveOrUpdate(serializable);
        em.getTransaction().commit();
    }

    public static <T>T findEntity(Class<T> c, String str) {
        return em.find(c, str);
    }

    public static <T> void removeEntity(Class<T> c, String str) {
        em.getTransaction().begin();
        em.remove(findEntity(c, str));
        em.getTransaction().commit();
    }

    public static List<?> findAllRows(Class<?> serializable) {
        return em.createQuery("FROM "+ serializable.getSimpleName(), serializable).getResultList();
    }
}
