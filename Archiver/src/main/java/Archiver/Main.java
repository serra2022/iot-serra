package Archiver;

import hibernate.GeneralManager;

public class Main {
    public static final String BROKER_URL = "ssl://127.0.0.1:8883"/*"tcp://smartcity-challenge.edu-al.unipmn.it:1883"*/;

    public static void main(String[] args) {
        GeneralManager.initHibernate();
        Subscriber sub1 = new Subscriber("to/+/archiver");
        sub1.start();
        ArchiverCloud.start();
    }

}