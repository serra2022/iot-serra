package Archiver;

import org.eclipse.paho.client.mqttv3.*;

import java.util.Date;

public class Publisher {

    private MqttTopic mqtt;
    private MqttClient client;
    private final String clientId = new Date().getTime() + "-pub";

    public Publisher(String topic) {
        System.out.println(clientId);
        String hurl = Main.BROKER_URL;
        System.out.println("connecting to: "+ hurl);

        //We have to generate a unique Client id.
        try {
            client = new MqttClient(hurl, clientId,null);
            mqtt = client.getTopic(topic);
        } catch (MqttException e) {
            e.printStackTrace();
            System.exit(1);
        }
    }

    public String getID() {
        return clientId;
    }

    public void start() {
        try {
            MqttConnectOptions options = new MqttConnectOptions();
            options.setCleanSession(false);
            options.setUserName("gruppo11");
            options.setPassword("funziona".toCharArray());
            options.setWill(client.getTopic(mqtt.getName()), "I'm gone :(".getBytes(), 0, false);

            client.connect(options);

        } catch (Exception e) {
            e.printStackTrace();
            System.exit(1);
        }
    }

    public void publish(String message) {
        try {
            mqtt.publish(new MqttMessage(message.getBytes()));
        } catch (MqttException e) {
            e.printStackTrace();
        }
        System.out.println("Messaggio pubblicato ["+message+"]");
    }
}
