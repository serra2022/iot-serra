package Archiver;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "sensors")
public class Sensors implements Serializable {

    public void setTemperature(Float temperature) {
        this.temperature = temperature;
    }

    public void setAirHumidity(Float airHumidity) {
        this.airHumidity = airHumidity;
    }

    public void setPressure(Float pressure) {
        this.pressure = pressure;
    }

    public void setSoilHumidity(Float soilHumidity) {
        this.soilHumidity = soilHumidity;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public Float getTemperature() {
        return temperature;
    }

    public Float getAirHumidity() {
        return airHumidity;
    }

    public Float getPressure() {
        return pressure;
    }

    public Float getSoilHumidity() {
        return soilHumidity;
    }

    public String getTime() {
        return time;
    }

    @Column(name = "temperature", length = 64)
    public Float temperature;

    @Column(name = "airHumidity", length = 64)
    public  Float airHumidity;

    @Column(name = "pressure", length = 64)
    public Float pressure;

    @Column(name = "soilHumidity", length = 64)
    public Float soilHumidity;

    @Id
    @Column(name = "time", length = 64)
    public String time;

    public Sensors() {}

    public Sensors(Float temp, Float air, Float pres, Float soil, String t) {
        this.temperature = temp;
        this.airHumidity = air;
        this.pressure = pres;
        this.soilHumidity = soil;
        this.time=t;
    }

}