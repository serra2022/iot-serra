package Archiver;

import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;

import javax.net.ssl.SSLSocketFactory;
import java.util.Date;

public class Subscriber {

    public final String topic;
    //We have to generate a unique Client id.
    String clientId = new Date().getTime() + "-sub";
    private MqttClient mqttClient;

    public Subscriber(String topic) {
        System.out.println(clientId);
        String hurl = Main.BROKER_URL;
        this.topic = topic;
        try {
            mqttClient = new MqttClient(hurl, clientId,null);
        } catch (MqttException e) {
            e.printStackTrace();
            System.exit(1);
        }
    }

    public String getID() {
        return clientId;
    }

    public void start() {
        try {
            mqttClient.setCallback(new SubscribeCallback());
            MqttConnectOptions options = new MqttConnectOptions();
            SSLSocketFactory socketFactory;
            try {
                socketFactory = Utility.getSocketFactory("./certs/caGruppo11.crt", "./certs/clientGruppo11.crt", "./certs/clientGruppo11.key", "");
                options.setSocketFactory(socketFactory);
            } catch (Exception e) {
                e.printStackTrace();
            }

            options.setUserName("gruppo11");
            options.setPassword("funziona".toCharArray());
            mqttClient.connect(options);

            //Subscribe to all subtopics of home
            mqttClient.subscribe(topic);

            System.out.println("Subscriber is now listening");

        } catch (MqttException e) {
            e.printStackTrace();
            System.exit(1);
        }
    }
}
