package Archiver;

import hibernate.GeneralManager;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

public class ArchiverCloud {

    static Publisher p = new Publisher("from/gruppo11/archiver");

    public static void start() {
        p.start();
    }

    public static void saveOnDB(String message) {
        try {
            JSONObject json = new JSONObject(message);
            System.out.println(message);
            if (json.has("temperature")) {
                Sensors s = new Sensors(json.getFloat("temperature"), json.getFloat("airHumidity"),
                        json.getFloat("pressure"), json.getFloat("soilHumidity"), json.getString("time"));
                GeneralManager.saveRow(s);
            }
            else if (json.has("level")){
                Water w = new Water(json.getInt("level"), json.getInt("statusLight"),
                        json.getBoolean("pump"), json.getString("time"));
                GeneralManager.saveRow(w);
            }
        } catch (JSONException e) { System.err.println(e.getLocalizedMessage());}
    }

    public static void getData() {
        List<Sensors> sensors = (List<Sensors>) GeneralManager.findAllRows(Sensors.class);
        List<Water> water = (List<Water>) GeneralManager.findAllRows(Water.class);
        p.publish(new JSONArray(sensors).toString());
        p.publish(new JSONArray(water).toString());
    }

}
