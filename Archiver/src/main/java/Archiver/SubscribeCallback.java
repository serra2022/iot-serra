package Archiver;

import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttMessage;

public class SubscribeCallback implements MqttCallback {

    public void connectionLost(Throwable cause) {
        System.err.println("LOST CONNECTION. CAUSED BY: " + cause.getMessage());
        cause.printStackTrace();
    }

    public void messageArrived(String topic, MqttMessage message) {
        System.out.println("Message arrived. Topic: " + topic + "  Message: " + message.toString());
        if (message.toString().startsWith("{\"domain\"") && !message.toString().contains("I'm gone :("))
            ArchiverCloud.getData();
        else
            ArchiverCloud.saveOnDB(message.toString());
    }

    public void deliveryComplete(IMqttDeliveryToken token) {
        //no-op
    }
}