package Archiver;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "water")
public class Water implements Serializable {

    @Column(name = "level", length = 64)
    public int level;

    @Column(name = "statusLight", length = 64)
    public int statusLight;

    @Column(name = "pump", length = 64)
    public Boolean pump;

    @Id
    @Column(name = "time", length = 64)
    public String time;

    public void setLevel(int level) {
        this.level = level;
    }

    public void setStatusLight(int statusLight) {
        this.statusLight = statusLight;
    }

    public void setPump(Boolean pump) {
        this.pump = pump;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public int getLevel() {
        return level;
    }

    public int getStatusLight() {
        return statusLight;
    }

    public Boolean getPump() {
        return pump;
    }

    public String getTime() {
        return time;
    }

    public Water(int level, int status, boolean pump, String t) {
        this.level=level;
        this.pump=pump;
        this.time=t;
        if(status<3)
            this.statusLight=status;
        else
            this.statusLight=0;
    }

    public Water(){
    }

}
