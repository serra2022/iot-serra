public class Sensors {

    public Float temperature;
    public Float airHumidity;
    public Float pressure;
    public Float soilHumidity;

    public Boolean fan;
    public String time;

    public Float getTemperature() {return temperature;}

    public Float getAirHumidity() {
        return airHumidity;
    }

    public Float getPressure() {
        return pressure;
    }

    public Float getSoilHumidity() {
        return soilHumidity;
    }

    public Boolean getFan() {
        return fan;
    }

    public String getTime() {return time;}

    public Sensors(){
        this.temperature = 0F;
        this.airHumidity = 0F;
        this.pressure = 0F;
        this.soilHumidity = 0F;
        this.fan=false;
    }

    public Sensors(Float temp, Float air, Float pres, Float soil, Boolean fan, String time){
        this.temperature = temp;
        this.airHumidity = air;
        this.pressure = pres;
        this.soilHumidity = soil;
        this.fan = fan;
        this.time = time;
    }

    public boolean equals(Sensors tmp) {
        return this.temperature.equals(tmp.temperature)
                && this.airHumidity.equals(tmp.airHumidity)
                && this.pressure.equals(tmp.pressure)
                && this.soilHumidity.equals(tmp.soilHumidity)
                && this.fan.equals(tmp.fan) ;
    }

    @Override
    public String toString() {
        return "Sensors {" +
                "temperature=" + temperature +
                ", air humidity=" + airHumidity +
                ", pressure=" + pressure +
                ", soil humidity=" + soilHumidity +
                ", fan=" + fan +
                ", time=" + time +
                "} ";
    }
}
