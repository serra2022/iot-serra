public class Water {

    public int level;
    public int statusLight;
    public Boolean pump;
    public String time;

    public String getTime() { return time; }

    public int getLevel() {
        return level;
    }

    public int getStatusLight() {
        return statusLight;
    }

    public Boolean getPump() {
        return pump;
    }

    public Water(int level, int status, boolean pump, String time) {
        this.level=level;
        this.pump=pump;
        if(status<3) {
            this.statusLight=status;
        } else {
            this.statusLight=0;
        }
        this.time = time;
    }

    public boolean equals(Water tmp) {
        return this.level == tmp.level && this.statusLight == tmp.statusLight && this.pump.equals(tmp.pump);
    }

    public Water(){
        this.level=0;
        this.pump=false;
        statusLight=0;
    }

    @Override
    public String toString() {
        return "Water {" + "level=" + level + ", pump=" + pump + ", statusLight=" + statusLight + "} ";
    }

}