import org.apache.commons.io.input.ReversedLinesFileReader;
import org.json.JSONObject;

import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class Dao {
    static Water water = new Water();
    static Publisher pub = new Publisher("to/", "archiver");
    static ScheduledExecutorService exec = Executors.newScheduledThreadPool(1);
    static Runnable writeWater = Dao::writer;
    static Runnable saveOnDb = Dao::saveOnDb;

    static public void setCurrentValue(String message) {
        JSONObject json = new JSONObject(message);
        if(json.has("level"))
            water.level = json.getInt("level");
        else if(json.has("statusLight"))
            water.statusLight = json.getInt("statusLight");
        else if(json.has("pump"))
            water.pump = json.getBoolean("pump");
        System.out.println(water.toString());
    }

    public static void startFileWritings() {
        exec.scheduleAtFixedRate(writeWater, 1, 1, TimeUnit.MINUTES);
    }

    public static void startSavingsOnDB() {
        exec.scheduleAtFixedRate(saveOnDb, 1, 5, TimeUnit.MINUTES);
        pub.start();
    }

    private static void writer() {
        try (PrintWriter out = new PrintWriter(new FileWriter("water.txt",true), true)) {
            water.time = LocalDateTime.now(ZoneId.of("Europe/Rome")).format(DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm"));
            JSONObject json = new JSONObject(water);
            System.out.println(json);
            Water empty = new Water();
            if (!empty.equals(water))
                out.println(json);
        } catch (Exception e) {
            e.printStackTrace();
        }
        ArrayList<Object> water = reader(3);
        System.out.println(water);
    }

    public static ArrayList<Object> reader(int n){
        ArrayList<Object> returnedData = new ArrayList<>();
        try (ReversedLinesFileReader input = new ReversedLinesFileReader(new File("water.txt"), StandardCharsets.UTF_8)) {
            int counter = 0;
            while(counter < n) {
                String tmp2 = input.readLine();
                if (tmp2 != null) {
                    JSONObject json = new JSONObject(tmp2);
                    try {
                        Water tmp = new Water(json.getInt("level"), json.getInt("statusLight"),
                                json.getBoolean("pump"), json.getString("time"));
                        returnedData.add(tmp);
                    }
                    catch (Exception e) {
                       //e.printStackTrace();
                    }
                }
                counter++;
            }
        }
        catch (Exception ignored) {}

        return returnedData;
    }

    public static void saveOnDb() {
        Water w = (Water) averageOfLast5MinutesWater();
        JSONObject json = new JSONObject(w);
        System.out.println("water" + json);
        pub.publish(json.toString());
    }

    public static Object averageOfLast5MinutesWater() {
        ArrayList<Object> water = reader(5);
        float level = 0F;
        boolean pump = false;
        for (Object obj : water) {
            Water tmp = (Water) obj;
            level += tmp.level;
            if (tmp.pump)
                pump = true;
        }
        int res = (int) (level / water.size());
        int light;
        if (res >= 150)
            light = 0;
        else if (res >= 50)
            light = 1;
        else
            light = 2;
        return new Water(res, light, pump,
                LocalDateTime.now(ZoneId.of("Europe/Rome")).format(DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm")));
    }

    public static void getCurrentValues() {
        Publisher pub = new Publisher("from/", "");
        pub.start();
        JSONObject json = new JSONObject(water);
        pub.publish(json.toString());
    }

    /*public static void getCurrentValues() {
        Publisher pub = new Publisher("from/", "");
        pub.start();
        JSONObject json = new JSONObject(water);
        pub.publish(json.toString());
    }*/

    public static ArrayList<Sensors> readerSensors(int n) {
        ArrayList<Sensors> returnedData = new ArrayList<>();
        try (ReversedLinesFileReader input = new ReversedLinesFileReader(new File("sensors.txt"), StandardCharsets.UTF_8)) {
            int counter = 0;
            while(counter < n) {
                String tmp2 = input.readLine();
                if (tmp2 != null) {
                    JSONObject json = new JSONObject(tmp2);
                    try {
                        Sensors tmp = new Sensors(json.getFloat("temperature"),
                                json.getFloat("airHumidity"),
                                json.getFloat("pressure"),json.getFloat("soilHumidity"), json.getBoolean("fan"), json.getString("time"));
                        returnedData.add(tmp);
                    }
                    catch (Exception e) {
                        //e.printStackTrace();
                    }
                }
                counter++;
            }
        }
        catch (Exception ignored) {}

        return returnedData;
    }

    public static Sensors averageOfLast5MinutesSensors() {
        ArrayList<Sensors> sensors = readerSensors(5);
        float temp = 0F, airHum = 0F, pres = 0F, soilHum = 0F;
        boolean fan = false;
        for (Sensors obj : sensors) {
            System.out.println(obj);
            temp += obj.temperature;
            airHum += obj.airHumidity;
            pres += obj.pressure;
            soilHum += obj.soilHumidity;
            if (obj.fan = true)
                fan = true;
        }
        return new Sensors(temp / sensors.size(), airHum / sensors.size(),
                pres / sensors.size(), soilHum / sensors.size(), fan,
                LocalDateTime.now(ZoneId.of("Europe/Rome")).format(DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm")));
    }

}
