import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.json.JSONObject;

import java.io.IOException;


public class SubscribeCallback implements MqttCallback {

    public void connectionLost(Throwable cause) {
        cause.printStackTrace();
        System.err.println("LOST CONNECTION. CAUSED BY: " + cause.getMessage());
    }

    public void messageArrived(String topic, MqttMessage message) throws IOException {
        System.out.println("Message arrived. Topic: " + topic + "  Message: " + message.toString());
        if (topic.startsWith("from")){ //from/water/*
            Dao.setCurrentValue(message.toString());
            if (message.toString().contains("pump")) // se è un messaggio della pompa
                WaterManager.sendPumpStatus(message.toString());
        }
        else if(topic.startsWith("rpc")) {
            if (topic.contains("data"))
                Dao.getCurrentValues();
            if (topic.contains("config"))
              WaterManager.readConfig();
        }
        else if (topic.contains("config"))
            WaterManager.updateConfig(message.toString());
        else
            WaterManager.openClose(new JSONObject(message.toString()).getInt("open"));
    }

    public void deliveryComplete(IMqttDeliveryToken token) {
        //no-op
    }
}