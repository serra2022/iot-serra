import java.time.LocalDateTime;
import java.util.Calendar;
import java.util.TimeZone;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

public class Scheduler {

    private final int hour;
    private final int minute;
    private final int second;
    private final TimerTask task;

    @Override
    public String toString() {
        return "Scheduler{" +
                "hour=" + hour +
                ", minute=" + minute +
                ", second=" + second +
                ", task=" + task +
                '}';
    }

    public Scheduler (int h, int min, int sec, TimerTask job){
        this.hour=h;
        this.minute=min;
        this.second=sec;
        this.task= job;
        this.run();
    }

    private void run(){
        Calendar today = Calendar.getInstance(TimeZone.getTimeZone("Europe/Rome"));
        today.set(Calendar.HOUR_OF_DAY, this.hour);
        today.set(Calendar.MINUTE, this.minute);
        today.set(Calendar.SECOND, this.second);

        Timer timer = new Timer();
        timer.schedule(task, today.getTime(), TimeUnit.MILLISECONDS.convert(1, TimeUnit.DAYS));
        System.out.println("pippo" + today.getTime() + LocalDateTime.now());
    }

}