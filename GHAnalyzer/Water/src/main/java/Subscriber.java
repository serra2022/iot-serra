import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import java.util.Date;

public class Subscriber {

    public String topic;
    //We have to generate a unique Client id.
    String clientId = new Date().getTime() + "-sub";
    private MqttClient mqttClient;

    public Subscriber(String pref, String suff) {
        String hurl = Netmon.BROKER_URL;
        String mainTopic = Netmon.MAIN_TOPIC;
        if (pref.equals("to/") || pref.equals("rpc/"))
            this.topic = pref + mainTopic + suff + "#";
        else
            this.topic = pref + suff;
        try {
            mqttClient = new MqttClient(hurl, clientId,null);
        } catch (MqttException e) {
            e.printStackTrace();
            System.exit(1);
        }
    }

    public String getID() {
        return clientId;
    }

    public void start() {
        try {
            mqttClient.setCallback(new SubscribeCallback());
            MqttConnectOptions options = setUpConnectionOptions();
            mqttClient.connect(options);
            //Subscribe to all subtopics of home
            if (topic.endsWith("/"))
                topic = topic.substring(0, topic.length()-1);
            mqttClient.subscribe(topic);

            System.out.println("Subscriber is now listening on " + topic);

        } catch (MqttException e) {
            e.printStackTrace();
            System.exit(1);
        }
    }

    private static MqttConnectOptions setUpConnectionOptions() {
        MqttConnectOptions options = new MqttConnectOptions();
        options.setCleanSession(true);
        options.setUserName("gruppo11");
        options.setPassword("funziona".toCharArray());
        return options;
    }
}
