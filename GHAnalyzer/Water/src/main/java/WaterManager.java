import org.apache.commons.io.IOUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.*;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.*;

import static java.lang.Integer.parseInt;

public class WaterManager {

    enum Type {
        onlyWater,
        waterSensors,
        all
    }

    static ScheduledExecutorService exec = Executors.newScheduledThreadPool(1);

    static List<ScheduledFuture> scheduledIrrigations = new ArrayList<>();
    static ScheduledThreadPoolExecutor sched = new ScheduledThreadPoolExecutor(1);
    static ScheduledExecutorService execIrrigations = Executors.newScheduledThreadPool(1);
    static Runnable check = WaterManager::checkWater;
    static Runnable close = WaterManager::closePump;
    static Runnable irrigation = WaterManager::IfIrrigate;
    private static Runnable createRunnable(final int duration){
        return () -> openClose(duration);
    }

    static Publisher pubLight = new Publisher("to/", "water/statusLight"); //to/water/statusLight
    static Publisher pubPump = new Publisher("to/", "water/pump"); //to/water/pump

    static Type type;

    private final static String FILE_CONF = "config.json";


    /**
     * Fa partire tutte le attività del WaterManager.
     * Controlla il tipo e in base a quello fa partire la configurazione corretta del manager
     */
    public static void start() throws IOException {
        sched.setRemoveOnCancelPolicy(true);
        //updateConfig("");
        JSONObject config = new JSONObject(leggiFile(FILE_CONF));

        switch(config.getString("type")) {
            case "onlyWater":
                type = Type.onlyWater;
                break;
            case "waterSensors":
                type = Type.waterSensors;
                break;
            default:
                type = Type.all;
        }
        System.out.println(type);
        pubLight.start();
        pubPump.start();
        exec.scheduleAtFixedRate(check, 0, 1, TimeUnit.MINUTES);


        Dao.startFileWritings();

        if(type == Type.all) {
            Dao.startSavingsOnDB();
            scheduledIrrigations.add(sched.scheduleAtFixedRate(irrigation, 0, 2, TimeUnit.HOURS));
        }
        else if(type == Type.onlyWater) {
            createScheduledIrrigations(config);
            /*JSONObject jo1 = new JSONObject();
            jo1.put("time", "17:05");
            jo1.put("duration", "20");

            JSONObject jo2 = new JSONObject();
            jo2.put("time", "18:05");
            jo2.put("duration", "30");

            JSONArray ja = new JSONArray();
            ja.put(jo1);
            ja.put(jo2);
            WaterManager.updateConfig(ja.toString());*/
        }
        else {
           /* TimerTask t1 = new TimerTask() {
                public void run() {
                    openClose(30);
                }
            };*/
            //new Scheduler(18,0,0,t1);
            scheduledIrrigations.add(sched.scheduleAtFixedRate(irrigation, 0, 2, TimeUnit.HOURS));
            //parlare con febbio dell'idea dei controlli continui(ogni ora?) P.S. febbio approva
        }
    }

    /**
     * Schedula le irrigazioni da svolgere agli orari prestabiliti nel caso in cui il tipo sia onlyWater
     *
     * @param config JsonObject contenente le irrigazioni da schedulare
     */
    private static void createScheduledIrrigations(JSONObject config) {
        System.out.println("size prima:" + sched.getQueue().size());
        for (ScheduledFuture s : scheduledIrrigations)
            s.cancel(false);
        scheduledIrrigations.clear();
        JSONArray scheduled_irrigations;
        try {
            scheduled_irrigations = config.getJSONArray("scheduled_irrigations");
        } catch (JSONException e) {
            return;
        }
        for (int i = 0; i < scheduled_irrigations.length(); i++) {
            JSONObject curr = scheduled_irrigations.getJSONObject(i);
            String time = curr.getString("time");
            int duration = curr.getInt("duration");
            LocalDateTime now = LocalDateTime.now();
            LocalDateTime ldt = LocalDateTime.of(now.getYear(), now.getMonthValue(), now.getDayOfMonth(),
                    parseInt(time.split(":")[0]), parseInt(time.split(":")[1]));
            // la bg è ritardata pertanto se si testa sul pc cambiare l'offset di now a 1
            long initialDelay = ldt.toInstant(ZoneOffset.of("+01:00")).toEpochMilli() - now.toInstant(ZoneOffset.of("+00:00")).toEpochMilli();
            if (initialDelay < 0) { //check per evitare di schedulare irrigazioni nel passato(si aggiunge un giorno per evitare)
                initialDelay = 86400000 - Math.abs(initialDelay);
            }
            System.out.println("now" + now);
            System.out.println("irrigazione schedulata" + now.plus(initialDelay, ChronoUnit.MILLIS));
            scheduledIrrigations.add(sched.scheduleAtFixedRate(createRunnable(duration), initialDelay, 86400000, TimeUnit.MILLISECONDS));
        }
        System.out.println("size dopo:" + sched.getQueue().size());
        for (ScheduledFuture s : scheduledIrrigations)
            System.out.println(s.getDelay(TimeUnit.MINUTES));

    }

    /**
     * Controlla ogni minuto il valore del livello dell'acqua nel serbatoio e pubblica il valore per gli arduino
     */
    private static void checkWater() {
        ArrayList<Object> values = Dao.reader(1);
        Water tmp= (Water) values.get(0);
        int light;
        if (tmp.level >= 150)
            light = 0;
        else if (tmp.level >= 50)
            light = 1;
        else
            light = 2;
        pubLight.publish(Integer.toString(light));
        System.out.println("statusLight:" + light);
    }

    /**
     * Verifica ogni 2 ore se le condizioni ambientali della serra indicano il bisogno di irrigare.
     * Se questo si verifica viene decisa la quantità di tempo per la quale la pompa rimarrà aperta.
     */
    private static void IfIrrigate() {
        Sensors sensors = Dao.averageOfLast5MinutesSensors();
        if (sensors.temperature >= 5 && sensors.temperature <= 30) {
            if (sensors.soilHumidity <= 18) {
                if (sensors.airHumidity <= 50)
                    openClose(50);
                else if (sensors.airHumidity <= 70)
                    openClose(30);
            } else if (sensors.soilHumidity <= 25) {
                if (sensors.airHumidity <= 50)
                    openClose(20);
                else if (sensors.airHumidity <= 70)
                    openClose(10);
            }
        }
    }

    /**
     * Pubblica sul topic il comando per azionare la pompa e ne schedula la chiusura dopo la duration in input
     *
     * @param duration intero che indica il tempo nel quale la pompa rimarrà aperta
     */
    public static void openClose(int duration) {
        //fare il check se l'acqua è già aperta
        new Thread(() -> {
            pubPump.publish("true");
            exec.schedule(close, duration, TimeUnit.SECONDS);
        }).start();
    }

    /**
     * Pubblica sul topic il comando per fermare la pompa
     */
    private static void closePump() {
        pubPump.publish("false");
    }

    /**
     * Pubblica sul topic lo stato della pompa che verrà consegnato alla webApp
     *
     * @param message stringa che contiene il messaggio da pubblicare
     */
    public static void sendPumpStatus(String message) {
        Publisher pub = new Publisher("from/", "pump"); //from/gruppo11/serra/water/pump
        pub.start();
        pub.publish(message);
    }

    /**
     * Aggiorna il file di configurazione con le nuove irrigazioni schedulate dall'utente
     *
     * @param message stringa che contiene la nuova configurazione
     */
    public static void updateConfig(String message) throws IOException {
        //String message2 = "[{\"time\":\"06:06\",\"duration\":\"5\"}]";
        JSONObject config = new JSONObject(leggiFile(FILE_CONF));
        System.out.println("prima" + config);
        System.out.println("message " + message);
        try (PrintWriter out = new PrintWriter(new FileWriter("config.json",false), true)) {
            try {
                if(Objects.equals(message, "[]"))
                    config.remove("scheduled_irrigations");
                else
                    config.put("scheduled_irrigations", new JSONArray(message));
            } catch (JSONException e) {
                System.out.println("catchato");
            }
            System.out.println("dopo" + config);
            out.println(config);
            Publisher pub = new Publisher("from/", "config");
            pub.start();
            pub.publish(message);
            createScheduledIrrigations(config);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Probabilmente non serve
     */
    public static void readConfig() throws IOException {
        File yourFile = new File("config.json");
        yourFile.createNewFile();
        try(FileInputStream inputStream = new FileInputStream("config.json")) {
            String config = IOUtils.toString(inputStream);
            System.out.println(config);
            Publisher pub = new Publisher("from/", "config");
            pub.start();
            pub.publish(config);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Legge il file di configurazione
     *
     * @param path stringa che contiene il path del file da leggere
     */
    public static String leggiFile(String path) throws IOException {
        String line;
        StringBuffer answer = new StringBuffer();
        BufferedReader bufferedReader = null;
        try {
            bufferedReader = new BufferedReader(new FileReader(path));
            while((line = bufferedReader.readLine()) != null) {
                answer.append(line).append("\n");
            }
        } catch (FileNotFoundException | EOFException ex) {
            ex.printStackTrace();
        } finally {
            if(bufferedReader != null)
                bufferedReader.close();
        }
        return answer.toString();
    }
}
