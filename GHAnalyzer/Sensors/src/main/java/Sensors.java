public class Sensors {

    public Double temperature;
    public Double airHumidity;
    public Double pressure;
    public Double soilHumidity;

    public Boolean fan;
    public String time;

    public Double getTemperature() {return temperature;}

    public Double getAirHumidity() {
        return airHumidity;
    }

    public Double getPressure() {
        return pressure;
    }

    public Double getSoilHumidity() {
        return soilHumidity;
    }

    public Boolean getFan() {
        return fan;
    }

    public String getTime() {return time;}

    public Sensors(){
        this.temperature = 0.0;
        this.airHumidity = 0.0;
        this.pressure = 0.0;
        this.soilHumidity = 0.0;
        this.fan=false;
    }

    public Sensors(Double temp, Double air, Double pres, Double soil, Boolean fan, String time){
        this.temperature = temp;
        this.airHumidity = air;
        this.pressure = pres;
        this.soilHumidity = soil;
        this.fan = fan;
        this.time = time;
    }

    public boolean equals(Sensors tmp) {
        return this.temperature.equals(tmp.temperature)
                && this.airHumidity.equals(tmp.airHumidity)
                && this.pressure.equals(tmp.pressure)
                && this.soilHumidity.equals(tmp.soilHumidity)
                && this.fan.equals(tmp.fan) ;
    }

    @Override
    public String toString() {
        return "Sensors {" +
                "temperature=" + temperature +
                ", air humidity=" + airHumidity +
                ", pressure=" + pressure +
                ", soil humidity=" + soilHumidity +
                ", fan=" + fan +
                ", time=" + time +
                "} ";
    }
}
