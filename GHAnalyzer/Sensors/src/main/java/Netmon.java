import java.io.IOException;

public class Netmon {
    public static final String BROKER_URL = "tcp://127.0.0.1:1883";
    public static final String MAIN_TOPIC = "gruppo11/serra/sensors/"; // domain(nome del gruppo)/sub-domain(il dominio sulla web app)/service (sensor, archiver, ...)

    static Subscriber subLocale = new Subscriber("from/","sensors");
    static Subscriber subRemote = new Subscriber("to/", "");
    static Subscriber rpc = new Subscriber("rpc/", "");

    public static void main(String[] args) {
        subLocale.start();
        subRemote.start();
        rpc.start();
        //fare il check se hai l'archiver (quando capiremo come fare install carina con il conf)
        Dao.startTimer();
        FanManager.start();
        //pub.start();
    }

}
