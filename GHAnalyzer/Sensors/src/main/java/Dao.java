import org.apache.commons.io.input.ReversedLinesFileReader;
import org.json.JSONObject;

import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class Dao {

    static Sensors sensors = new Sensors();
    static Publisher pub = new Publisher("to/","archiver");
    static ScheduledExecutorService exec = Executors.newScheduledThreadPool(1);
    static Runnable writeSensors = Dao::writer;
    static Runnable saveOnDb = Dao::saveOnDb;

    static public void setCurrentValue(String message) {
        try {
            JSONObject json = new JSONObject(message);
            if (json.has("temperature"))
                sensors.temperature = json.getDouble("temperature");
            else if (json.has("airHumidity"))
                sensors.airHumidity = json.getDouble("airHumidity");
            else if (json.has("pressure"))
                sensors.pressure = json.getDouble("pressure");
            else if (json.has("soilHumidity"))
                sensors.soilHumidity = json.getDouble("soilHumidity");
            else if (json.has("fan"))
                sensors.fan = json.getBoolean("fan");
            System.out.println(sensors.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void startTimer() {
        exec.scheduleAtFixedRate(writeSensors, 1, 1, TimeUnit.MINUTES);
        exec.scheduleAtFixedRate(saveOnDb, 1, 5, TimeUnit.MINUTES);
        pub.start();
    }

    private static void writer() {
        try (PrintWriter out = new PrintWriter(new FileWriter("sensors.txt",true), true)) {
            sensors.time = LocalDateTime.now(ZoneId.of("Europe/Rome")).format(DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm"));
            JSONObject json = new JSONObject(sensors);
            Sensors empty = new Sensors();
            if (!empty.equals(sensors)) {
                System.out.println("new line added" + json);
                out.println(json);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        ArrayList<Sensors> sensor = reader(3);
        System.out.println("last 3 rows from file " + sensor);
    }

    public static ArrayList<Sensors> reader(int n) {
        ArrayList<Sensors> returnedData = new ArrayList<>();
        try (ReversedLinesFileReader input = new ReversedLinesFileReader(new File("sensors.txt"), StandardCharsets.UTF_8)) {
            int counter = 0;
            while(counter < n) {
                String tmp2 = input.readLine();
                if (tmp2 != null) {
                    JSONObject json = new JSONObject(tmp2);
                    try {
                        Sensors tmp = new Sensors(json.getDouble("temperature"),
                                json.getDouble("airHumidity"),
                                json.getDouble("pressure"),json.getDouble("soilHumidity"), json.getBoolean("fan"), json.getString("time"));
                        returnedData.add(tmp);
                    }
                    catch (Exception e) {
                        //e.printStackTrace();
                    }
                }
                counter++;
            }
        }
        catch (Exception ignored) {}

        return returnedData;
    }

    public static void saveOnDb() {
        Sensors s = averageOfLast5Minutes();
        JSONObject json = new JSONObject(s);
        //json.put("name", domain/subdomain) quando avremo file carino
        System.out.println("sensors" + json);
        pub.publish(json.toString());
    }

    public static Sensors averageOfLast5Minutes() {
        ArrayList<Sensors> sensors = reader(5);
        float temp = 0F, airHum= 0F, pres= 0F, soilHum = 0F;
        boolean fan = false;
        for (Sensors obj : sensors) {
            System.out.println(obj);
            temp += obj.temperature;
            airHum += obj.airHumidity;
            pres += obj.pressure;
            soilHum += obj.soilHumidity;
            if (obj.fan = true)
                fan = true;
        }
        return new Sensors((double) (temp/sensors.size()), (double) (airHum/sensors.size()),
                (double) (pres/sensors.size()), (double) (soilHum/sensors.size()), fan,
                LocalDateTime.now(ZoneId.of("Europe/Rome")).format(DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm")));
    }

    public static void getCurrentValues() {
        Publisher pub = new Publisher("from/", "");
        pub.start();
        JSONObject json = new JSONObject(sensors);
        pub.publish(json.toString());
    }

}
