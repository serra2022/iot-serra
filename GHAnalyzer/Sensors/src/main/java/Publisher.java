import org.eclipse.paho.client.mqttv3.*;
import java.util.ArrayList;
import java.util.Date;

public class Publisher implements MqttCallbackExtended {

    private MqttTopic mqtt;
    private MqttClient client;
    private final String clientId = new Date().getTime() + "-pub";
    private ArrayList<String> offlineMessages = new ArrayList<>();

    public Publisher(String pref, String suff) {
        String hurl = Netmon.BROKER_URL;
        System.out.println("connecting to: "+ hurl);

        try {
            client = new MqttClient(hurl, clientId, null);
            if (suff.equals(""))
                mqtt = client.getTopic(pref + Netmon.MAIN_TOPIC.substring(0, Netmon.MAIN_TOPIC.length()-1));
            else if(suff.equals("fan"))
                mqtt = client.getTopic(pref + Netmon.MAIN_TOPIC + suff);
            else
                mqtt = client.getTopic(pref + suff);
        } catch (MqttException e) {
            e.printStackTrace();
            System.exit(1);
        }
    }

    public String getID() {
        return clientId;
    }

    public void start() {
        try {
            MqttConnectOptions options = new MqttConnectOptions();
            //options.setCleanSession(false);
            options.setWill(client.getTopic(mqtt.getName()), "I'm gone :(".getBytes(), 0, false);
            options.setUserName("gruppo11");
            options.setPassword("funziona".toCharArray());
            options.setAutomaticReconnect(true);
            options.setConnectionTimeout(10);
            client.setCallback(this);
            System.out.println(client.getTopic(mqtt.getName()));
            client.connect(options);
        } catch (MqttException e) {
            e.printStackTrace();
            System.exit(1);
        }
    }

    public void publish(String message) {
        try {
            MqttMessage m = new MqttMessage(message.getBytes());
            m.setQos(1);
            MqttDeliveryToken token = mqtt.publish(m);
            new Thread(() -> {
                try {
                    Thread.sleep(10000L);
                    System.out.println(token.isComplete());
                    if (!token.isComplete())
                        offlineMessages.add(message);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            }).start();
        } catch (MqttException e) {
            e.printStackTrace();
        }
        System.out.println(mqtt.getName() + " Messaggio pubblicato ["+message+"]");
    }

    @Override
    public void connectComplete(boolean arg0, String arg1) {
        for (String s: offlineMessages) {
            publish(s);
        }
        offlineMessages.clear();
    }

    public void connectionLost(Throwable cause) {}

    public void deliveryComplete(IMqttDeliveryToken token) {}

    public void messageArrived(String topic, MqttMessage message) {}

}