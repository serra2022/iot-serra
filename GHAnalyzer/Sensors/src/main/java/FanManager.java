import org.apache.commons.io.IOUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.*;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.*;

import static java.lang.Integer.parseInt;

public class FanManager {

    static ScheduledExecutorService exec = Executors.newScheduledThreadPool(1);
    static Runnable close = FanManager::closeFan;

    static Runnable cooling = FanManager::IfCooling;

    static Publisher pubFan = new Publisher("to/", "sensors/fan"); //to/sensors/fan


    /**
     * Fa partire tutte le attività del FanManager.
     */
    public static void start() {
        exec.scheduleAtFixedRate(cooling, 0, 1, TimeUnit.HOURS);
        pubFan.start();
        //openClose(10);
    }

    /**
     * Verifica ogni ora se le condizioni ambientali della serra indicano il bisogno di attivare la ventola.
     * Se questo si verifica viene decisa la quantità di tempo per la quale la ventola rimarrà aperta.
     */
    private static void IfCooling() {
        Sensors sensors = Dao.averageOfLast5Minutes();
        if (sensors.temperature >= 40 || sensors.airHumidity >= 80)
            openClose(60);
        else if (sensors.temperature >= 35 || sensors.airHumidity >= 65)
            openClose(45);
        else if(sensors.temperature >= 30 || sensors.airHumidity >= 50)
            openClose(30);
        else if(sensors.temperature >= 25 || sensors.airHumidity >= 40)
            openClose(10);
    }

    /**
     * Pubblica sul topic il comando per azionare la ventola e ne schedula la chiusura dopo la duration in input
     *
     * @param duration intero che indica il tempo nel quale la ventola rimarrà aperta
     */
    public static void openClose(int duration) {
        //fare il check se la ventola è già attiva
        new Thread(() -> {
            pubFan.publish("true");
            exec.schedule(close, duration, TimeUnit.SECONDS);
        }).start();
    }

    /**
     * Pubblica sul topic il comando per fermare la ventola
     */
    private static void closeFan() {
        pubFan.publish("false");
    }

    /**
     * Pubblica sul topic lo stato della pompa che verrà consegnato alla webApp
     *
     * @param message stringa che contiene il messaggio da pubblicare
     */
    public static void sendFanStatus(String message) {
        Publisher pub = new Publisher("from/", "fan"); //from/gruppo11/serra/sensors/fan
        pub.start();
        pub.publish(message);
    }
}
