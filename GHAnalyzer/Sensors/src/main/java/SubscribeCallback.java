import org.eclipse.paho.client.mqttv3.*;
import org.json.JSONObject;


public class SubscribeCallback implements MqttCallback {

    public void connectionLost(Throwable cause) {
        System.err.println("LOST CONNECTION. CAUSED BY: ");
        cause.printStackTrace();
    }

    public void messageArrived(String topic, MqttMessage message) {
        System.out.println("Message arrived. Topic: " + topic + "  Message: " + message.toString());
        if (topic.startsWith("from")) {
            Dao.setCurrentValue(message.toString());
            if (message.toString().contains("fan")) // se è un messaggio della ventola
                FanManager.sendFanStatus(message.toString());
        }
        else if (topic.startsWith("rpc")) {
            Dao.getCurrentValues();
        } else
            try {
                FanManager.openClose(new JSONObject(message.toString()).getInt("open"));
            } catch (Exception e) {
                e.printStackTrace();
            }
    }

    public void deliveryComplete(IMqttDeliveryToken token) {
        //no-op
    }
}