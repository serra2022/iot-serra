#!/bin/bash
javac -cp .:./org.json-1.0.0.v201011060100.jar:./org.apache.commons.io.jar:./gson-2.8.2.jar:./commons-codec-1.15/commons-codec-1.15.jar -d bin src/code/*
echo 'server compiled'
# shellcheck disable=SC2164
cd bin
java -classpath .:../org.json-1.0.0.v201011060100.jar:../org.apache.commons.io.jar:../gson-2.8.2.jar:../commons-codec-1.15/commons-codec-1.15.jar code.Server $1
