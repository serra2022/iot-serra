'use strict';
import {getToken} from "./script.js";
import Domain from "./domain.js";

import {getDomainURL} from './configuration.js';

class RequestToDomain {

    /**
    * Funzione per ottenenere dal domain manager tutte le informazioni
    * sui domini dell'utente che ha effettuato il login
    */
    static async getMyDomains() {
        const responseDomain = await fetch(`${getDomainURL()}secured/domains/`, {  // DA FARE: controllare che l'uri che ho specificato qui coincida con quello specificato dal domain manager
            method: 'GET',
            headers: {
                'Authorization': `Bearer ${await getToken()}`
            }
        });
        const domainsJson = await responseDomain.json();
        if(responseDomain.ok) {
            const domainsArray = domainsJson.response;
            return domainsArray.map (d => {
                return Domain.from (d);
            });
        }
        else
            throw domainsJson;
    }


    /**
     * Effettua la richiesta per creare un nuovo dominio.
     * @param {*} json descrizione json del dominio che si vuole creare.
     * @returns true se il dominio &egrave; stato creato,
     * false altrimenti.
     */
    static async createNewDomain(json) {
        const response = await fetch(`${getDomainURL()}install/`, {
            method: 'POST',
            headers: {
                'Authorization': `Bearer ${await getToken()}`
            },
            body: JSON.stringify(json)
        });
        return response.ok;
    }


    /**
     * Effettua la richiesta al domain manager per ottenere tutti i sevizi
     * disponibili.
     */
    static async getAllServices() {
        const responseDomain = await fetch(`${getDomainURL()}secured/services`, {  // DA FARE: controllare che l'uri che ho specificato qui coincida con quello specificato dal domain manager
            method: 'GET',
            headers: {
                'Authorization': `Bearer ${await getToken()}`
            }
        }) ;
        const servicesJson = await responseDomain.json();
        if(responseDomain.ok) {
            return servicesJson.response;
        }
        else
            throw servicesJson;
    }

    /**
     * Effettua la richiesta al domain manager per ottenere tutti i servizi in uso dal dominio
     * @param {*} domain
     * @returns
     */
     static async getUsedServices(domain) {
        const response = await fetch(`${getDomainURL()}secured/services?domain=${domain}`, {
            method: 'GET',
            headers: {
                'Authorization': `Bearer ${await getToken()}`
            }
        });
        const servicesJson = await response.json();//{response: ["servizio1", "servizio2"]};
        if(response.ok) {
            return servicesJson.response;
        }
        else
            throw servicesJson;
    }

    /**
     * Effettua la richiesta al domain manager per far partire un dominio.
     * @param {*} d dominio che si vuole far partire.
     * @returns true se il dominio &egrave; stato fatto partire,
     * false altrimenti.
     */
     static async startDomain(d) {
        const response = await fetch(`${getDomainURL()}start/`, {
            method: 'POST',
            headers: {
                'Authorization': `Bearer ${await getToken()}`
            },
            body: JSON.stringify({domain: d.nome})
        });
        return response.ok;
    }


    /**
     * Effettua la richiesta al domain manager per fermare un dominio.
     * @param {*} d dominio che si vuole fermare.
     * @returns true se il dominio &egrave; stato fermato,
     * false altrimenti.
     */
     static async stopDomain(d) {
        const response = await fetch(`${getDomainURL()}stop/`, {
            method: 'POST',
            headers: {
                'Authorization': `Bearer ${await getToken()}`
            },
            body: JSON.stringify({domain: d.nome})
        });
        return response.ok;
    }


    /**
     * Effettua la richiesta al domain manager per cancellare un dominio.
     * @param {*} d dominio che si vuole cancellare.
     * @returns true se la cancellazione &egrave; andata a buon fine,
     * false altrimenti.
     */
    static async deleteDomain(d) {
        const response = await fetch(`${getDomainURL()}delete/`, {
            method: 'POST',
            headers: {
                'Authorization': `Bearer ${await getToken()}`
            },
            body: JSON.stringify({domain: d.nome})
        });
        return response.ok;
    }
    /**
     * Effettua la richiesta al domain manager per sapere se l'utente e' un amministratore del dominio o meno
     * @param {*} domain
     * @returns
     */
    static async getUserPriviledges(domain) {
        const response = await fetch(`${getDomainURL()}secured/priviledges?domain=${domain}`, {
            method: 'GET',
            headers: {
                'Authorization': `Bearer ${await getToken()}`
            }
        });
        const privilegesJson = await response.json();// String response = "{\"priviledges\":\"admin\"}" || "{\"priviledges\":\"user\"}";
        if(response.ok) {
            return privilegesJson.priviledges;
        }
        else
            throw privilegesJson;
    }
}

export default RequestToDomain;
