'use strict';
import {createIrrigationTime, createRowDomain, selectDiv} from '../templates/domains-template.js';
import { logoutKeycloak } from './script.js';
import {createNewUser, createNewService, createControlServices} from '../templates/create-new-domain-template.js';
import RequestToDomain from './requests-to-domain.js';
import Api from "../mqtt/api.js";
import { Tipo } from "./domain.js"

export let domainType
class App {
    constructor(myDomains) {
        // this.myDomains = myDomains;
        this.showAllDomains(myDomains);
        this.fillModal();
        this.performLogout();
    }

    showAllDomains(domainsToShow) {
        const addHere = document.getElementById('table-row-domains');
        for(const d of domainsToShow) {
            this.showSingleDomain(d,addHere);
        }
    }

    showSingleDomain(domainObject, addHere) {
        const container = addHere == null ? document.getElementById('table-row-domains') : addHere;
        const div = document.createElement('div');
        div.classList.add("domain-div");
        div.innerHTML = createRowDomain(domainObject);
        const plusRow = container.lastElementChild;
        container.insertBefore(div, plusRow);
        if(domainObject.admin) {
            const toggle = div.querySelector('.toggle-button');
            this.statoDomainToggle(domainObject, toggle);
            const deleteDomain = div.querySelector('.fa-trash');
            deleteDomain.addEventListener('click', async (event) => {
                deleteDomain.classList.add("spinner-border");
                deleteDomain.classList.remove("fa-trash");
                deleteDomain.setAttribute("disabled", "")
                event.preventDefault()
                event.stopPropagation();

                const resultDelete = await RequestToDomain.deleteDomain(domainObject);
                if(resultDelete) {
                    container.removeChild(div);
                }
                else {
                    // throw new Error('Impossibile eliminare il dominio, provare piu\' tardi');
                    deleteDomain.classList.remove("spinner-border");
                    deleteDomain.classList.add("fa-trash");
                    deleteDomain.removeAttribute("disabled");
                    this.showAlert('Errore eliminazione', 'Impossibile eliminare il dominio, provare piu\' tardi', false);
                }
            });
        }

        div.addEventListener('click', (event) => {
            event.preventDefault();
            event.stopPropagation();
            let inter
            if(domainObject.stato) {
                //document.getElementById("load-not-possible").classList.add('invisible')
                if (!div.classList.contains ('current')) {
                    document.getElementById ('table-row-domains').querySelectorAll ('.current').forEach (el => {
                        el.classList.remove ('current');
                    });
                    div.classList.add ('current');
                    sessionStorage.setItem ('domain', domainObject.nome);
                    //document.getElementById('no-services').classList.remove("invisible");
                    /*const div2 = document.createElement('div');
                    div2.innerHTML = contentAll();*/
                    let all = document.getElementById ("all");
                    if (document.getElementById ("content") == null)
                        all.querySelector ('#loading').insertAdjacentElement ("afterend", selectDiv (domainObject.tipo));
                    else {
                        document.getElementById ("content").replaceWith (selectDiv (domainObject.tipo));
                        document.getElementById ("content").classList.add('invisible')
                    }

                    if (domainObject.tipo === Tipo.AllPackages || domainObject.tipo === Tipo.OnlyWater || domainObject.tipo === Tipo.WaterSensors) {
                        let btnIrrigate = document.getElementById ("btnIrrigate");
                        btnIrrigate.addEventListener ('click', (ev) => {
                            ev.preventDefault ();
                            ev.stopPropagation ();
                            //btnIrrigate.disabled = true;
                            Api.openPump (10);
                            /*let timeLeft = 10;
                            const downloadTimer = setInterval (function () {
                                if (timeLeft <= 0) {
                                    clearInterval (downloadTimer);
                                    btnIrrigate.innerText = "IRRIGA";
                                } else {
                                    btnIrrigate.innerText = "IRRIGANDO... " + timeLeft;
                                }
                                timeLeft -= 1;
                            }, 1000);*/
                        });
                    }
                    if (domainObject.tipo === Tipo.AllPackages || domainObject.tipo === Tipo.WaterSensors ||
                        domainObject.tipo === Tipo.OnlySensors || domainObject.tipo === Tipo.SensorsArchiver) {
                        let btnCooling = document.getElementById ("btnCooling");
                        btnCooling.addEventListener ('click', (ev) => {
                            ev.preventDefault ();
                            ev.stopPropagation ();
                            //btnCooling.disabled = true;
                            Api.startFan (10);
                            /*let timeLeft = 10;
                            const downloadTimer = setInterval (function () {
                                if (timeLeft <= 0) {
                                    clearInterval (downloadTimer);
                                    btnCooling.innerText = "ARIEGGIA";
                                } else {
                                    btnCooling.innerText = "ARIEGGIANDO... " + timeLeft;
                                }
                                timeLeft -= 1;
                            }, 1000);*/
                        });
                    }
                    if (domainObject.tipo === Tipo.OnlyWater) {
                        const divC = document.getElementById ('content')
                        divC.querySelector ('#add-hour').addEventListener ('click', () => {
                            if (divC.querySelector ('#header').classList.contains ('d-none')) {
                                divC.querySelector ('#header').classList.remove ('d-none')
                                divC.querySelector ('#noHours').classList.add ('d-none')
                            }
                            const dd = document.createElement ('div')
                            dd.innerHTML = createIrrigationTime ()
                            divC.querySelector ('#hours').appendChild (dd)
                            dd.querySelector ('#remove-hour').addEventListener ('click', () => {
                                dd.remove ()
                                if (divC.querySelector ('#hours').children.length === 2) {
                                    divC.querySelector ('#header').classList.add ('d-none')
                                    divC.querySelector ('#noHours').classList.remove ('d-none')
                                }
                            })
                        })
                        divC.querySelector ('#save-hours').addEventListener ('click', () => {
                            const jArray = []

                            const hours = Array.from (divC.querySelectorAll ('#hour'));
                            const durations = Array.from (divC.querySelectorAll ('#duration'));
                            const hs = hours.map ((h) => {
                                if (h.value === '')
                                    return h
                            });
                            const ds = durations.map ((d) => {
                                if (d.value === '' || isNaN (parseInt (d.value)))
                                    return d
                            });
                            console.log (hs, ds)

                            if (hs.every ((el) => el === undefined) && ds.every ((el) => el === undefined)) {
                                for (let i = 0; i < hours.length; i++) {
                                    jArray[i] = {'time': hours[i].value, 'duration': durations[i].value}
                                }
                                Api.sendWaterConfig (JSON.stringify (jArray))
                            } else {
                                for (let i = 0; i < hours.length; i++) {
                                    if (hs.includes (hours[i]))
                                        hours[i].classList.add ('border-danger');
                                    else
                                        hours[i].classList.remove ('border-danger');
                                    if (ds.includes (durations[i]))
                                        durations[i].classList.add ('border-danger');
                                    else
                                        durations[i].classList.remove ('border-danger');
                                }
                            }
                        })
                    }

                    intervalApis (domainObject, true)
                    inter = setInterval (() => {
                        console.log('100')
                        if(domainObject.stato)
                            intervalApis (domainObject, false)
                        else
                            clearInterval(inter)
                    }, 15000)
                    document.getElementById ("loading").classList.remove ("invisible");
                    //window.location.href = a.href;
                }
            } else {
                clearInterval(inter)
                document.getElementById ("loading").classList.add ("invisible");
                document.getElementById("load-not-possible").classList.remove('invisible')
                const domainOff = document.getElementById('domain-off')
                domainOff.innerHTML = domainObject.admin === true ? 'Domain spento, accendilo' : 'Domain spento, chiedi a un amministratore di accenderlo'
            }
            document.querySelectorAll('#close-content-div').forEach( el => {
                el.addEventListener('click', () => {
                    clearInterval(inter)
                    el.parentElement.classList.add('invisible')
                    document.getElementById ('table-row-domains').querySelectorAll ('.current').forEach (el => {
                        el.classList.remove ('current');
                    });
                })
            })
        });
    }

    statoDomainToggle(domain, toggle) {
        toggle.addEventListener('click', async (event) => {
            event.preventDefault();
            event.stopPropagation();
            const stop = toggle.classList.contains('active');
            const linkDomain = toggle.parentElement.parentElement.parentElement;
            if(stop) {
                domain.stato = false
                const resultStop = await RequestToDomain.stopDomain(domain);
                // toggle attiva. Se clicco richiedo che il dominio sia fermato
                if(resultStop) {
                    toggle.classList.remove('active');
                    linkDomain.setAttribute('href','#');
                }
                else {
                    // throw new Error('Impossibile fermare il dominio, provare piu\' tardi');
                    this.showAlert('Errore', 'Impossibile fermare il dominio, provare piu\' tardi', false);
                }
                if(document.getElementById('content') != null)
                    document.getElementById('content').classList.add('invisible')
                document.getElementById('loading').classList.add('invisible')
                document.getElementById ('table-row-domains').querySelectorAll ('.current').forEach (el => {
                    el.classList.remove ('current');
                });
            } else {
                domainType = domain.tipo
                domain.stato = true
                document.getElementById('load-not-possible').classList.add('invisible')
                const resultStart = await RequestToDomain.startDomain(domain);
                if(resultStart) {
                    toggle.classList.add('active');
                    linkDomain.setAttribute('href','/secured/home/');
                }
                else {
                    // throw new Error('Impossibile far partire il dominio, provare piu\' tardi');
                    this.showAlert('Errore', 'Impossibile far partire il dominio, provare piu\' tardi', false);
                }
            }
        });
    }

    async fillModal() {
        const allServices = await RequestToDomain.getAllServices();
        const div = document.createElement('div');
        div.id = "checkbox-div";
        div.classList.add('pb-2', 'd-flex', 'justify-content-around');
        for(const s of allServices) {
            div.insertAdjacentHTML('beforeend', createNewService(s));
        }

        const modalBody = document.getElementById('modal-body-new-domain');
        modalBody.appendChild(div);
        div.insertAdjacentHTML('afterend', createControlServices());
        //modalBody.insertAdjacentHTML('beforeend', createNewUser());

        let userNum = 0;// una riga e' gia' presente
        const addUserButton = document.getElementById('add-user-button');
        addUserButton.addEventListener('click', () => {
            const div = document.createElement('div')
            div.innerHTML = createNewUser(userNum)
            modalBody.insertAdjacentElement('beforeend', div);
            div.querySelector(`#delete-user${userNum}-field`).addEventListener('click', () => {
                div.remove();
                userNum--;
            })
            userNum++;
        });

        const form = document.getElementById('new-domain-form');
        const cancelForm = document.getElementById('cancel-button');
        form.addEventListener('submit', async (event) => {
            event.preventDefault();

            let errors = 0;
            const domainNameInput = document.getElementById('nuovoDominio');
            const domainName = domainNameInput.value.trim();
            if(domainName === ''){
                errors++;
                domainNameInput.classList.add('is-invalid');
                domainNameInput.classList.remove('is-valid');
                document.getElementById('valid-domainName').innerHTML = '';
                document.getElementById('invalid-domainName').innerHTML = 'Inserire un nome';
            }
            else {
                domainNameInput.classList.add('is-valid');
                domainNameInput.classList.remove('is-invalid');
                document.getElementById('valid-domainName').innerHTML = 'Ok';
                document.getElementById('invalid-domainName').innerHTML = '';
            }

            const topicsArr = [ {role:"A", topic: `+/${domainName}/#`} ];
            const checkboxarr = document.querySelectorAll("input.form-check-input");
            const checks = [];  // array contentente i nomi dei servizi che l'utente vuole nel dominio che sta creando
            for(const c of checkboxarr){
                if(c.checked) {
                    checks.push(c.value);
                    topicsArr.push({role:"U", topic: `to/${domainName}/serra/${c.value}/#`});
                    topicsArr.push({role:"U", topic: `from/${domainName}/serra/${c.value}/#`});
                    topicsArr.push({role:"U", topic: `rpc/${domainName}/serra/${c.value}/#`});
                }
            }

            const checkboxDiv = document.getElementById('checkbox-div');
            
            if(checks.length === 0){
                errors++;
                checkboxDiv.classList.add('is-invalid');
                checkboxDiv.classList.remove('is-valid');
                document.getElementById('valid-services').innerHTML = '';
                document.getElementById('invalid-services').innerHTML = 'Selezionare almeno un servizio';
            }
            else { 
                if(checks.includes('archiver') && !checks.includes('sensors')) {
                    errors++;
                    checkboxDiv.classList.add('is-invalid');
                    checkboxDiv.classList.remove('is-valid');
                    document.getElementById('valid-services').innerHTML = '';
                    document.getElementById('invalid-services').innerHTML = 'Non e\' possibile avere l\'archiver senza il microservizio sensors';
                } else {
                    checkboxDiv.classList.add('is-valid');
                    checkboxDiv.classList.remove('is-invalid');
                    document.getElementById('valid-services').innerHTML = 'Ok';
                    document.getElementById('invalid-services').innerHTML = '';
                }
            }

            let i=0;
            const utenti = [];
            while(i<userNum){
                const nomeInput = document.getElementById(`nuovoUtente${i}`);
                const nome = nomeInput.value;

                if(nome==='') {
                    errors++;
                    nomeInput.classList.add('is-invalid');
                    nomeInput.classList.remove('is-valid');
                    document.getElementById(`valid-userName${i}`).innerHTML = '';
                    document.getElementById(`invalid-userName${i}`).innerHTML = 'Inserire il nome dell\'utente';
                }
                else {
                    nomeInput.classList.add('is-valid');
                    nomeInput.classList.remove('is-invalid');
                    document.getElementById(`valid-userName${i}`).innerHTML = 'Ok';
                    document.getElementById(`invalid-userName${i}`).innerHTML = '';
                }

                const passwordInput = document.getElementById(`passwordUtente${i}`);
                const password = passwordInput.value;
                if(password==='') {
                    errors++;
                    passwordInput.classList.add('is-invalid');
                    passwordInput.classList.remove('is-valid');
                    document.getElementById(`valid-userPassword${i}`).innerHTML = '';
                    document.getElementById(`invalid-userPassword${i}`).innerHTML = 'Inserire la password per l\'utente';
                }
                else {
                    passwordInput.classList.add('is-valid');
                    passwordInput.classList.remove('is-invalid');
                    document.getElementById(`valid-userPassword${i}`).innerHTML = 'Ok';
                    document.getElementById(`invalid-userPassword${i}`).innerHTML = '';
                }

                const ruoloInput = document.getElementById(`ruoloUtente${i}`);
                const ruolo = ruoloInput.value;
                if(ruolo === 'unselected') {
                    errors++;
                    ruoloInput.classList.add('is-invalid');
                    ruoloInput.classList.remove('is-valid');
                    document.getElementById(`valid-userRole${i}`).innerHTML = '';
                    document.getElementById(`invalid-userRole${i}`).innerHTML = 'Specificare il ruolo dell\'utente';
                }
                else {
                    ruoloInput.classList.add('is-valid');
                    ruoloInput.classList.remove('is-invalid');
                    document.getElementById(`valid-userRole${i}`).innerHTML = 'Ok';
                    document.getElementById(`invalid-userRole${i}`).innerHTML = '';
                }

                utenti.push({
                    user: nome,
                    role: ruolo,
                    passwd: password
                });
                i++;
            }

            if(errors > 0) {
                return;
            }
            /*document.getElementById("btnSubmit").attributes("disabled", true);*/
            //event.submitter.setAttribute("disabled", "true");
            document.getElementById("btnSubmit").disabled = true
            const json = {
                domain: domainName,
                services: checks,
                users: utenti,
                topics: topicsArr
            };
            cancelForm.click();
            document.getElementById("btnSubmit").removeAttribute("disabled")
            const resultInstall = await RequestToDomain.createNewDomain(json);

            if(resultInstall) {
                const domainCreated = {
                    nome: domainName,
                    stato: false,
                    admin: true,
                    tipo: getDomainType(checks)
                };
                this.showSingleDomain(domainCreated);
            }
            else {
                // throw new Error('Impossibile creare il dominio, provare piu\' tardi');
                this.showAlert('Dominio esistente','Impossibile creare il dominio, provare con un altro nome', false);
            }
            //event.submitter.setAttribute("disabled", "false");
        });


        cancelForm.addEventListener('click', () => {
            form.reset();
            document.getElementById('nuovoDominio').classList.remove('is-valid');
            document.getElementById('nuovoDominio').classList.remove('is-invalid');
            document.getElementById('valid-domainName').innerHTML = '';
            document.getElementById('invalid-domainName').innerHTML = '';

            document.getElementById('valid-services').innerHTML = '';
            document.getElementById('invalid-services').innerHTML = '';

            // serve userNum -1 perche' subito dopo aver creato la riga, userNum viene incrementato
            // si va fino a 0 perche' la prima riga viene mantenuta
            for(let i=(userNum-1); i>0; i--){
                const divRow = document.getElementById(`utente${i}`);
                const parent = divRow.parentNode;
                parent.removeChild(divRow);
            }
            userNum = 0;
            const userRow = document.getElementById(`utente${userNum}`);
            if(userRow != null) {
                const inputs = userRow.querySelectorAll ('input');
                for (const input of inputs) {
                    input.classList.remove ('is-valid');
                    input.classList.remove ('is-invalid');
                }
                const feedback = [...userRow.querySelectorAll ('div > div.valid-feedback')];
                feedback.push (...userRow.querySelectorAll ('div > div.invalid-feedback'));
                for (const f of feedback) {
                    f.innerHTML = '';
                }
                const select = userRow.querySelector ('select');
                select.classList.remove ('is-valid');
                select.classList.remove ('is-invalid');
            }
        });
    }

    performLogout() {
        const buttonLogout = document.getElementById('button-logout');
        buttonLogout.addEventListener('click', async (event) => {
            event.preventDefault();
            await logoutKeycloak ();
        });
    }

    showAlert(head, body, green) {
        const alert = document.getElementById('alert-message');
        if(alert == null)
            throw new Error("Alert box non trovata. Errore: " + body);
        green ? alert.classList.add('alert-success') : alert.classList.add('alert-danger');
        alert.innerHTML = `
        <div class="d-flex justify-content-around container-fluid">
            ${green ? '<i class="fa-solid fa-check-circle fa-4x" aria-hidden="true"></i>' : '<i class="fa-solid fa-exclamation-circle fa-4x" aria-hidden="true"></i>'}
            <div>
                <h4 class="alert-heading">${head}</h4>
                <p>${body}</p>
            </div>
        </div>`;//<i class="fa-solid fa-triangle-exclamation fa-fade"></i>
        alert.classList.remove('invisible');
        // dismiss the alert after 3 seconds
        setTimeout(() => {
            alert.classList.add('invisible');
            alert.classList.remove('alert-success');
            alert.classList.remove('alert-danger');
            alert.innerHTML = "";
        }, 3000);
    };

}

function getDomainType(check) {
    if (check.length === 3)
        return 0;
    else if (check.length === 2)
        if(check.includes("archiver"))
            return 1;
        else
            return 2;
    else
    if (check.includes("water"))
        return 3;
    else
        return 4;
}

function intervalApis(domainObject, confBool) {
    switch (domainObject.tipo) {
        case Tipo.AllPackages: {
            Api.getCurrentWaterValue ();
            Api.getCurrentSensorsValue ();
            Api.getData ();
            break;
        }
        case Tipo.SensorsArchiver: {
            Api.getCurrentSensorsValue ();
            Api.getData ();
            break;
        }
        case Tipo.WaterSensors: {
            Api.getCurrentWaterValue ();
            Api.getCurrentSensorsValue ();
            break;
        }
        case Tipo.OnlyWater: {
            Api.getCurrentWaterValue ();
            if(confBool)
                Api.getCurrentWaterConfig();
            break;
        }
        case Tipo.OnlySensors: {
            Api.getCurrentSensorsValue ();
            break;
        }
    }
}

export default App;