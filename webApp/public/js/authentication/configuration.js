"use strict";

const conf = {
	"self-server-url": "https://localhost:3000/",
	"keycloak": {
		"realm": "pippo",
		"base-server-url": "http://localhost:8080/"
	},
	"domain": {
		"base-server-url": "https://localhost:3001/"
	}
};

function getMyURL() {
	return conf["self-server-url"];
}

function getKeycloakInfo() {
	return conf.keycloak;
}

function getKeycloakRealm() {
	return conf.keycloak.realm;
}

function getKeycloakURL() {
	return conf.keycloak["base-server-url"];
}

function getDomainURL() {
	return conf.domain["base-server-url"];
}

export { getMyURL, getKeycloakInfo, getKeycloakRealm, getKeycloakURL, getDomainURL };