'use strict';

export const Tipo = {
    AllPackages: 0,
    SensorsArchiver: 1,
    WaterSensors: 2,
    OnlyWater: 3,
    OnlySensors: 4
}

class Domain {
    constructor(nome, stato, admin, tipo) {
        this.nome = nome; // nome del dominio
        this.stato = stato; // on/off
        this.admin = admin; // true se l'utente attualmente loggato e' amministratore di questo dominio, false altrimenti
        if (tipo === 0)
            this.tipo = Tipo.AllPackages
        else if (tipo === 1)
            this.tipo = Tipo.SensorsArchiver
        else if (tipo === 2)
            this.tipo = Tipo.WaterSensors
        else if (tipo === 3)
            this.tipo = Tipo.OnlyWater
        else
            this.tipo = Tipo.OnlySensors
    }

    /**
     * Costruisce un oggetto Domain partendo dall'oggetto passato in input. 
     * @param {*} json oggetto dal quale si vuole creare un oggetto Domain.
     * @returns l'oggetto Domain creato.
     */
     static from(json) {
         console.log(Object.assign(new Domain(), json))
         return Object.assign(new Domain(), json);
    }

}

export default Domain;