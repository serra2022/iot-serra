'use strict';

class Secure {

    constructor() {
        this.codeVerifier = this.base64URLEncode(CryptoJS.lib.WordArray.random(50));
        this.state = this.base64URLEncode(CryptoJS.lib.WordArray.random(50));
        this.codeChallenge = this.base64URLEncode(CryptoJS.SHA256(this.codeVerifier));
    }

    base64URLEncode(str) {
        return CryptoJS.enc.Base64.stringify(str)
            .replace(/\+/g, '-')
            .replace(/\//g, '_')
            .replace(/=/g, '');
    }

}

export default Secure;