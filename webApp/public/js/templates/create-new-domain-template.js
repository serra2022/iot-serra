'use strict';

function createNewUser(count = 0) {
    return `<div class="d-flex justify-content-around align-items-center p-2" id="utente${count}">
                <div class="me-2">
                    <label for="#nuovoUtente${count}" class="form-label fw-bold">Nome</label>
                    <input type="text" class="form-control" id="nuovoUtente${count}" aria-describedby="#nuovoUtente${count}Help" placeholder="Inserisci il nome dell'utente">
                    <div id="valid-userName${count}" class="valid-feedback"></div>
                    <div id="invalid-userName${count}" class="invalid-feedback"></div>
                </div>
                <div class="me-2">
                    <label for="#passwordUtente${count}" class="form-label fw-bold">Password</label>
                    <input type="password" class="form-control" id="passwordUtente${count}" aria-describedby="#passwordUtente${count}Help" placeholder="Inserisci la password per l'utente">
                    <div id="valid-userPassword${count}" class="valid-feedback"></div>
                    <div id="invalid-userPassword${count}" class="invalid-feedback"></div>
                </div>
                <div class="me-2">
                    <label for="#ruoloUtente${count}" class="form-label fw-bold">Ruolo</label>
                    <select class="form-select" id="ruoloUtente${count}" name="ruolo">
                        <option value="unselected" selected>Scegli un ruolo</option>
                        <option value="U">Utente</option>
                        <option value="A">Amministratore</option>
                    </select>
                    <div id="valid-userRole${count}" class="valid-feedback"></div>
                    <div id="invalid-userRole${count}" class="invalid-feedback"></div>
                </div>
                <div class="mt-2">
                    <br>
                    <i id="delete-user${count}-field" class="fa-solid fa-circle-minus fa-2x"></i>
                </div>
            </div>`;
}


function createNewService(nomeServizio) {
    return `<div class="form-check">
                <input class="form-check-input" type="checkbox" value="${nomeServizio}" id="${nomeServizio}"/>
                <label class="form-check-label" for="#${nomeServizio}">
                    ${nomeServizio}
                </label>
            </div>`;
}


function createControlServices() {
    return `<div id="valid-services" class="valid-feedback"></div>
            <div id="invalid-services" class="invalid-feedback"></div>`;
}

export {createNewUser, createNewService, createControlServices};