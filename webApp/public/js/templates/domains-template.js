'use strict';

import { Tipo } from "../authentication/domain.js";

function createRowDomain(domain) {
    return `
    <!-- <a href=${domain.stato !== 0 ? '"/secured/"' : '"#"'} style="text-decoration: none"> -->
        <div class="d-flex align-items-center">
            <div class="me-auto px-2">
                <h4 class="d-flex justify-content-start mt-2 ms-1" style="color: #1f1f1f; font-weight: bold">${domain.nome}</h4>
                <p class="d-flex justify-content-start mt-1 ms-1">${domainTypeToString(domain.tipo)}</p>
            </div>
            <div class="px-2">
                <div class="toggle-button no-box-sizing ${domain.stato ? 'active' : ''}" style="rotate: -90deg">
                    <div class="inner-circle no-box-sizing"></div>
                </div>
            </div>
            <div class="px-2">
                 ${domain.admin === true ? '<i class="fa-solid fa-trash fa-2x py-2" style="color: #ff0000"></i>' : '<div class="trash-padding"></div>'}
            </div>
            
        </div>
    <!-- </a> -->`;
}

function selectDiv(type) {
    console.log("selectDiv tipo = ", type)
    if (type === Tipo.AllPackages)
        return contentAll()
    else if (type === Tipo.SensorsArchiver)
        return contentSensorsArchiver()
    else if (type === Tipo.WaterSensors)
        return contentSensorsWater()
    else if (type === Tipo.OnlyWater)
        return contentOnlyWater()
    else
        return contentOnlySensors()
}

function contentAll() {
    const div = document.createElement("div");
    div.setAttribute("id","content");
    div.classList.add("grid", "m-3", "p-3", "justify-content-center", 'invisible');
    div.innerHTML = `
        <p id="close-content-div" class="close-content-div">✕</p>
        <div class="box a gridItem p-3">
            <div class="a">
                <div class="border-item bg-light position-relative">
                     <label class="badge rounded-pill bg-primary position-absolute start-50 translate-middle-x mt-2">Water Level</label>
                     <br><p id="waterLevel" class="text-center align-middle" style="font-size:2vw"></p>
                </div>
            </div>
            <div class="b">
                <div class="border-item bg-light position-relative" >
                    <label  class="badge rounded-pill bg-primary position-absolute start-50 translate-middle-x mt-2">Soil Humidity</label>
                    <br><p id="soilHumidity" class="text-center align-middle" style="font-size:2vw"></p>
                </div>
            </div>
            <div class="c">
                <div class="border-item bg-light position-relative">
                    <label class="badge rounded-pill bg-primary position-absolute start-50 translate-middle-x mt-2">Air Humidity</label>
                    <br><p id="airHumidity" class="text-center align-middle" style="font-size:2vw"></p>
                </div>
            </div>
            <div class="d">
                <div class="border-item bg-light position-relative">
                    <label class="badge rounded-pill bg-primary position-absolute start-50 translate-middle-x mt-2">Temperature</label>
                    <br><p id="temperature" class="text-center align-middle" style="font-size:2vw"></p>
                </div>
            </div>
            <div class="e">
                <button class="btn bg-primary rounded-pill" id="btnIrrigate" style="width: 100%">IRRIGA</button>
                <div class="countdown" id="countdown"></div>
            </div>
            <div class="f">
                <button class="btn bg-primary rounded-pill" id="btnCooling" style="width: 100%">ARIEGGIA</button>
                <div class="countdown" id="countdown"></div>
            </div>
        </div>
        <div class="box b align-items-center">
            <canvas id="myChart1"></canvas>
        </div>
        <div class="box c ">
            <canvas id="myChart2"></canvas>
        </div>
        <div class="box d ">
            <canvas id="myChart3"></canvas>
        </div>`;
    return div;
}

function contentOnlyWater() {
    const div = document.createElement("div");
    div.setAttribute("id","content");
    div.classList.add("d-flex", "m-3", "p-3", 'invisible');
    div.innerHTML = `
    <p id="close-content-div" class="close-content-div">✕</p>
    <div style="width:100%" class="d-flex justify-content-between">
        <div style="width:50%" class="d-flex position-relative flex-column justify-content-between">
            <div id="hours" style="overflow-y: auto;height: 80%;">
                <h2 class="bold ms-2">Orari irrigazione</h2>
                <div id="header" class="d-flex m-2 d-none">
                    <span class="ms-4 me-5 ps-1">Orario</span>
                    <span class="ms-3 ps-1">Durate (in secondi)</span>
                </div>
            </div>
            <p id="noHours" class="text-center" style="font-size:2vw;position: absolute;top: 42%">Nessun orario d'irrigazione programmato</p>
            <div class="d-flex justify-content-evenly align-items-center">
                <button id="add-hour" type="button" class="btn btn-primary" data-dismiss="modal">Aggiungi orario</button>
                <button id="save-hours" type="button" class="btn btn-primary" data-dismiss="modal">Salva orari</button>
            </div>
        </div>

        <div style="width:40%" class="position-relative align-self-center">
            <label class="badge rounded-pill bg-primary position-absolute start-50 translate-middle-x" style="font-size: 2vw">Water Level</label>
            <br><p id="waterLevel" class="text-center align-middle mt-3" style="font-size:5vw"></p>
            <!--mandare indietro dalla beaglebone se l'acqua è aperta(timeout dopo un tot e snackbar carino)-->
            <br><button class="btn bg-primary rounded-pill irriga" style="width: 100%" id="btnIrrigate">IRRIGA</button> 
        </div>
    </div>`;
    return div;
}

function createIrrigationTime() {
    return `
        <div class="d-flex justify-content-evenly align-items-center align-items-center mb-2">
            <div class="form-group">
                <input id="hour" class="form-control" type="time">
            </div>
            <div class="form-group">
                <input id="duration" class="form-control" type="number" step="1" min="1">
            </div>
            <i id="remove-hour" class="fa-solid fa-circle-minus" style="font-size: 150%;"></i>
        </div>`;
}

function contentOnlySensors() {
    const div = document.createElement("div");
    div.setAttribute("id","content");
    div.classList.add('m-3', 'p-3', 'invisible');
    div.innerHTML = ` 
        <p id="close-content-div" class="close-content-div">✕</p>
        <div class="box a grid p-3" style="height: 100%">
            <div class="a">
                <div class="border-item bg-light position-relative" style="height: 100%; display: grid">
                    <label class="badge rounded-pill bg-primary position-absolute start-50 translate-middle-x mt-2" style="font-size: 2vw">Soil Humidity</label>
                    <br><p id="soilHumidity" class="bigItem" style="margin-top: 0"></p>
                </div>
            </div>
            <div class="b">
                <div class="border-item bg-light position-relative" style="height: 100%; display: grid">
                    <label class="badge rounded-pill bg-primary position-absolute start-50 translate-middle-x mt-2" style="font-size: 2vw">Air Humidity</label>
                    <br><p id="airHumidity" class="bigItem" style="margin-top: 0"></p>
                </div>
            </div>
            <div class="c">
                <div class="border-item bg-light position-relative" style="height: 100%; display: grid">
                    <label class="badge rounded-pill bg-primary position-absolute start-50 translate-middle-x mt-2" style="font-size: 2vw">Temperature</label>
                    <br><p id="temperature" class="bigItem" style="margin-top: 0"></p>
                </div>
            </div>
            <div class="d d-flex align-items-center">
                 <button class="btn bg-primary rounded-pill" id="btnCooling" style="width: 100%; font-size:3vw">ARIEGGIA</button>
            </div>
        </div>`;
    return div;
}

function contentSensorsWater() {
    const div = document.createElement("div");
    div.setAttribute("id","content");
    div.classList.add('m-3', 'p-3', 'invisible');
    div.innerHTML = `
    <p id="close-content-div" class="close-content-div">✕</p>
    <div class="box gridItem p-3" style="height: 100%">
        <div class="a">
            <div class="border-item bg-light position-relative" style="height: 100%; display: grid">
                <label class="badge rounded-pill bg-primary position-absolute start-50 translate-middle-x mt-2" style="font-size: 2vw">Water Level</label>
                <br><p id="waterLevel" class="bigItem" style="margin-top: 12% !important;"></p>
            </div>
        </div>
        <div class="b">
            <div class="border-item bg-light position-relative" style="height: 100%; display: grid">
                <label  class="badge rounded-pill bg-primary position-absolute start-50 translate-middle-x mt-2" style="font-size: 2vw">Soil Humidity</label>
                <br><p id="soilHumidity" class="bigItem"></p>
            </div>
        </div>
        <div class="c">
            <div class="border-item bg-light position-relative" style="height: 100%; display: grid">
                <label class="badge rounded-pill bg-primary position-absolute start-50 translate-middle-x mt-2" style="font-size: 2vw">Air Humidity</label>
                <br><p id="airHumidity" class="bigItem"></p>
            </div>
        </div>
        <div class="d">
            <div class="border-item bg-light position-relative" style="height: 100%; display: grid">
                <label class="badge rounded-pill bg-primary position-absolute start-50 translate-middle-x mt-2" style="font-size: 2vw">Temperature</label>
                <br><p id="temperature" class="bigItem"></p>
            </div>
        </div>
        <div class="e mb-3 d-flex">
            <button class="btn bg-primary rounded-pill" id="btnIrrigate" style="font-size: 2.5vw; width: 100%">IRRIGA</button>
        </div>
        <div class="f mb-3 d-flex">
            <button class="btn bg-primary rounded-pill" id="btnCooling" style="font-size: 2.5vw; width: 100%">ARIEGGIA</button>
        </div>
    </div>`;
    return div;
}

function contentSensorsArchiver() {
    const div = document.createElement("div");
    div.setAttribute("id","content");
    div.classList.add('grid', 'm-3', 'p-3', 'justify-content-center', 'invisible');
    div.innerHTML = `
        <p id="close-content-div" class="close-content-div">✕</p>
        <div class="box a grid p-3">
            <div class="a">
                <div class="border-item bg-light position-relative" >
                    <label class="badge rounded-pill bg-primary position-absolute start-50 translate-middle-x mt-2">Soil Humidity</label>
                    <br><p id="soilHumidity" class="text-center align-middle" style="font-size:3vw"></p>
                </div>
            </div>
            <div class="b">
                <div class="border-item bg-light position-relative">
                    <label class="badge rounded-pill bg-primary position-absolute start-50 translate-middle-x mt-2">Air Humidity</label>
                    <br><p id="airHumidity" class="text-center align-middle" style="font-size:3vw"></p>
                </div>
            </div>
            <div class="c">
                <div class="border-item bg-light position-relative">
                    <label class="badge rounded-pill bg-primary position-absolute start-50 translate-middle-x mt-2">Temperature</label>
                    <br><p id="temperature" class="text-center align-middle" style="font-size:3vw"></p>
                </div>
            </div>
            <div class="d d-flex align-items-center">
                 <button class="btn bg-primary rounded-pill" id="btnCooling" style="width: 100%; font-size:1.5vw">ARIEGGIA</button>
            </div>
        </div>
        <div class="box b align-items-center">
            <canvas id="myChart1"></canvas>
        </div>
        <div class="box c">
            <canvas id="myChart2"></canvas>
        </div>
        <div class="box d">
            <canvas id="myChart3"></canvas>
        </div>`;
    return div;
}

function domainTypeToString(tipo) {
    if (tipo === 0)
        return 'AllPackages'
    else if (tipo === 1)
        return 'SensorsArchiver'
    else if (tipo === 2)
        return 'WaterSensors'
    else if (tipo === 3)
        return 'OnlyWater'
    else
        return 'OnlySensors'
}

export {createRowDomain, createIrrigationTime, contentAll, contentOnlySensors, contentOnlyWater, contentSensorsWater, selectDiv, domainTypeToString};