"use strict";

import {all, sendMessage} from "./mqtthat.js";

class Api {

    static getData = () => {
        const topic = "to/gruppo11/archiver";
        const message = `{"domain":"gruppo11"}`;
        sendMessage(topic, message);
        all.wait.archiver = true;
    };

    static openPump = durata => {
        const topic = "to/gruppo11/serra/water";
        const message = `{"open":"${durata}"}`;
        sendMessage(topic, message);
    }

    static startFan = durata => {
        const topic = "to/gruppo11/serra/sensors";
        const message = `{"open":"${durata}"}`;
        sendMessage(topic, message);
    }

    static getCurrentWaterValue = () => {
        const topic = "rpc/gruppo11/serra/water/data";
        const message = `{"cmd":"data"}`;
        sendMessage(topic, message);
        all.wait.water = true;
    }

    static getCurrentSensorsValue = () => {
        const topic = "rpc/gruppo11/serra/sensors/data";
        const message = `{"cmd":"data"}`;
        sendMessage(topic, message);
        all.wait.sensors = true;
    }

    static sendWaterConfig = (jsonArray) => {
        const topic = "to/gruppo11/serra/water/config";
        sendMessage(topic, jsonArray);
    }

    static getCurrentWaterConfig = () => {
        const topic = "rpc/gruppo11/serra/water/config";
        const message = `{"cmd":"requestConfig"}`;
        sendMessage(topic, message);
    }

    static sendWaterConfigType = (type) => {    //all, onlyWater, waterSensors
        const topic = "from/gruppo11/webapp/water";
        const message = `{"type":${type}}`;
        sendMessage(topic, message);
    }

    static sendSensorsConfigType = (type) => {  //onlySensors, sensorsArchiver
        const topic = "from/gruppo11/webapp/sensors";
        const message = `{"type":${type}}`;
        sendMessage(topic, message);
    }

}


export default Api;