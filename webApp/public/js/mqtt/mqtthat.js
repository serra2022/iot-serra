"use strict";
import Api from "./api.js";
import {createIrrigationTime, domainTypeToString} from "../templates/domains-template.js";
import {domainType} from "../authentication/app.js";

const values = {
    sensors: false,
    water: false,
    archiver: false
}

export const all = {
    x:1,
    y:2,
    wait: values
}

// Create a client instance
const mqtt_domain = "gruppo11";
const mqtt_subdomain = "serra";
const mqtt_tree = mqtt_domain + "/" + mqtt_subdomain + "/";
export { mqtt_tree };
const d = new Date();
const clientid = "browser" + d.getTime();
// const host = location.host.split(":");
// const mqtthost = host[0];
const mqtthost = "127.0.0.1";

const client = new Paho.MQTT.Client(mqtthost, 9001, clientid);//first try => wss://luci.local:9001/mqtt
// key = public key of the server
// secret = random string

// const client = new Paho.MQTT.Client(`wss://key:secret@${mqtthost}/mqtt`, clientid);//should be the correct way of connecting
// const client = new Paho.MQTT.Client(`wss://key:secret@${mqtthost}`, clientid);

// set callback handlers
client.onConnectionLost = onConnectionLost;
client.onMessageArrived = onMessageArrived;
// client.onFailure = onFailure;
const connectOptions = {
    userName: "gruppo11",
    password: "funziona",
    useSSL: false,
    // cleanSession: false,
    onSuccess: onConnect,
    onFailure: onFailure
};
// connect the client
client.connect(connectOptions);

let isConnected = false;

function sendMessage(topic, message, retryNum = 0) {
    if (isConnected) {
        const message_ = new Paho.MQTT.Message(message);
        message_.destinationName = topic;
        client.send(message_);
    } else if (retryNum < 3) {
        setTimeout(() => {
            sendMessage(topic, message, retryNum++);
        }, 1000);
    } else {
        console.log('impossibile inviare il messaggio')
        throw new Error('impossibile inviare il messaggio');
    }
}

export { sendMessage };

// called when the client connects
function onConnect() {
    console.log("connected")
    // Once a connection has been made, make a subscription and send a message.

    //client.subscribe(`to/gruppo11/#`);
    client.subscribe(`+/gruppo11/#`);
    isConnected = true;
    // tell every module that we're connected
    Api.isConnected = true;
}

let dateConLost = null;
const maxTime = 1200000;
let retry = true;
function onFailure(message) {
    console.log("onFailure", message);
    dateConLost == null ? dateConLost = new Date() : null;
    console.log(dateConLost)
    let time = Math.abs(new Date().getTime() - dateConLost.getTime());
    console.log({retry,time})
    if (retry && (time < maxTime)) {
        client.connect(connectOptions);
    }
    if (time >= maxTime) {
        retry = false;
        console.log('Tentativo di riconnessione fallito');
    }
}

// called when the client loses its connection
function onConnectionLost(responseObject) {
    console.log("connection lost", responseObject);
    setTimeout(() => {
        client.connect(connectOptions);
    }, 1000);
}


// called when a message arrives
function onMessageArrived(message) {
    //console.log("topic " + message.topic, "payload " + message.payloadString);
    if (message.topic.startsWith(`rpc/gruppo11/webapp/sensors`)) {
        Api.sendSensorsConfigType(domainTypeToString(domainType))
    }
    else if (message.topic.startsWith(`rpc/gruppo11/webapp/water`)) {
        Api.sendWaterConfigType(domainTypeToString(domainType))
    }
    else if (message.topic.startsWith(`from/gruppo11/archiver`)) {
        let json = JSON.parse(message.payloadString);
        fillAllGraphs(json);
        all.wait.archiver = false;
        console.log("arc");
        console.log("water " + all.wait.water, "sensors " + all.wait.sensors)
        if(!all.wait.water && !all.wait.sensors) {
            document.getElementById('content').classList.remove('invisible')
            document.getElementById ("loading").classList.add ("invisible");
        }
    }
    else if (message.topic.startsWith("from/gruppo11/serra/water/config")) {
        fillWaterConfig(JSON.parse(message.payloadString))
    }
    else if (message.topic.startsWith(`from/gruppo11/serra/water/pump`)) {
        let json = JSON.parse(message.payloadString);
        let btnIrrigate = document.getElementById ("btnIrrigate");
        let irrigating
        let timer
        if (json["status"] === true) {
            if(btnIrrigate !== undefined) {
                irrigating = true
                btnIrrigate.disabled = true;
                let timeLeft = json["duration"];
                timer = setInterval (function () {
                    if (timeLeft <= 0) {
                        clearInterval (timer);
                        btnIrrigate.innerText = "IRRIGA";
                    } else if(irrigating){
                        btnIrrigate.innerText = "IRRIGANDO... " + timeLeft;
                    }
                    timeLeft -= 1;
                }, 1000);
            }
        } else if (json["status"] === false) {
            if(btnIrrigate !== undefined) {
                clearInterval(timer)
                irrigating = false
                btnIrrigate.innerText = "IRRIGA";
                btnIrrigate.attributes.removeNamedItem ("disabled");
            }
        }
    }
    else if (message.topic.startsWith(`from/gruppo11/serra/water/statusLight`)) {
        let json = JSON.parse(message.payloadString);
        const level = document.getElementById('waterLevel');
        if (json["statusLight"] === "0")
            level.innerText = "🟢";
        else if (json["statusLight"] === "1")
            level.innerText = "🟡";
        else
            level.innerText = "🔴";
    }
    else if (message.topic.startsWith(`from/gruppo11/serra/water`)) {
        let json = JSON.parse(message.payloadString);
        const level = document.getElementById('waterLevel');
        all.wait.water = false;
        console.log("wat");
        if(!all.wait.archiver && !all.wait.sensors) {
            document.getElementById('content').classList.remove('invisible')
            document.getElementById ("loading").classList.add ("invisible");
        }
        if (json["statusLight"] === 0)
            level.innerText = "🟢";
        else if (json["statusLight"] === 1)
            level.innerText = "🟡";
        else
            level.innerText = "🔴";
    }
    else if (message.topic.startsWith(`from/gruppo11/serra/sensors/fan`)) {
        let json = JSON.parse(message.payloadString);
        let btnCooling = document.getElementById("btnCooling");
        let cooling
        let timerF
        if (json["status"] === true) {
            if (btnCooling !== undefined){
                cooling = true
                btnCooling.disabled = true;
                let timeLeft = json["duration"];
                timerF = setInterval (function () {
                    if (timeLeft <= 0) {
                        clearInterval (timerF);
                        btnCooling.innerText = "ARIEGGIA";
                    } else if (cooling){
                        btnCooling.innerText = "ARIEGGIANDO... " + timeLeft;
                    }
                    timeLeft -= 1;
                }, 1000);
            }
        } else if (json["status"] === false) {
            if (btnCooling !== undefined) {
                cooling = false
                clearInterval(timerF)
                btnCooling.innerText = "ARIEGGIA";
                btnCooling.attributes.removeNamedItem ("disabled");
            }
        }
    }
    else if (message.topic.startsWith(`from/gruppo11/serra/sensors`)) {
        all.wait.sensors = false;
        if(!all.wait.archiver && !all.wait.water) {
            document.getElementById('content').classList.remove('invisible')
            document.getElementById ("loading").classList.add ("invisible");
        }
        let json = JSON.parse(message.payloadString);
        const temp = document.getElementById('temperature');
        const airHum = document.getElementById('airHumidity');
        const soilHum = document.getElementById('soilHumidity');
        temp.innerText = parseInt(json["temperature"].toString()).toFixed(1) + ' °C';
        airHum.innerText = parseInt(json["airHumidity"].toString()).toFixed(1) + ' %';
        soilHum.innerText = parseInt(json["soilHumidity"].toString()).toFixed(1) + ' %';
    }
    else
        console.log("else msg arrived", message.topic, message.payloadString);
}

function fillWaterConfig(json) {
    json.forEach(conf => {
        const d = document.createElement('div')
        d.innerHTML = createIrrigationTime()
        d.querySelector("#hour").value = conf["time"]
        d.querySelector("#duration").value = conf["duration"]
        const divC = document.getElementById('content')
        divC.querySelector('#hours').appendChild(d)
        d.querySelector('#remove-hour').addEventListener('click', () => {
            d.remove()
            if(divC.querySelector('#hours').children.length === 2) {
                divC.querySelector('#header').classList.add('d-none')
                divC.querySelector('#noHours').classList.remove('d-none')
            }
        })
        divC.querySelector('#header').classList.remove('d-none')
        divC.querySelector('#noHours').classList.add('d-none')
    })
}

// funzioni del prof
/*function cleanItem(item) {
    let citem = "";
    for (let i = 0; i < item.length; i++)
        if (item[i] !== '"' && item[i] !== '}' && item[i] !== '{' && item[i] !== '\n')
            citem += item[i];
    return citem;
}

function normalizeValue(value, digit) {
    let res = "";
    let n = 0;
    let dot = false;
    for (i = 0; i < value.length && n < digit; i++) {
        res += value[i];
        if (dot == true) n++;
        if (value[i] == '.') dot = true;
    }
    return res;
}

function getNamefromTag(name) {
    const names = name.split("-");
    if (names.length > 1)
        return names[1];
    else
        return "";
}

function button_update(name, value) {//this is pretty much useless now
    return;
    const id = "I_" + name;
    const image = document.getElementById(id);
    if (image == null) return;
    if (value == "false" || value < 1) {
        image.src = "/res/led-green.png"
    } else {
        image.src = "/res/led-red.png"
    }
    return;
}


function field_update(name, value) {
    const el = document.getElementById(name)
    if (el == null) return;
    el.innerHTML = value;
    // const pname = "#" + name;
    // $(pname).text(value);//questo non funziona
}

doClick = function (name) {
    const id = "I_" + name;
    const image = document.getElementById(id);
    if (image.src.includes("led-green.png")) {
        mqtt_message = new Paho.MQTT.Message("{cmd:1}");
        mqtt_message.destinationName = "to/" + mqtt_domain + "/" + mqtt_subdomain + "/gpio/" + name;
        client.send(mqtt_message);
    }
    else {
        mqtt_message = new Paho.MQTT.Message("{cmd:0}");
        mqtt_message.destinationName = "to/" + mqtt_domain + "/" + mqtt_subdomain + "/gpio/" + name;
        client.send(mqtt_message);
    }
    return;
}*/

function fillAllGraphs(json) {
    const ctx1 = document.getElementById('myChart1').getContext('2d');
    const ctx2 = document.getElementById('myChart2').getContext('2d');
    const ctx3 = document.getElementById('myChart3').getContext('2d');

    let arrTemp = [];
    let arrSoil = [];
    let arrAir = [];
    let arrTime = [];
    for(let j of json)
        if (j.temperature != null) {
            arrTemp.push(parseFloat(j.temperature).toFixed(1).toString());
            arrSoil.push(parseFloat(j.soilHumidity).toFixed(1).toString());
            arrAir.push(parseFloat(j.airHumidity).toFixed(1).toString());
            arrTime.push(j.time);
        }

    console.log(arrTemp);
    fillGraph(ctx1, "Temperatures", arrTime, arrTemp);
    fillGraph(ctx2, "Air Humidity", arrTime, arrAir);
    fillGraph(ctx3, "Soil Humidity", arrTime, arrSoil);
}

function fillGraph(ctx, label, labels, data) {
    new Chart(ctx, {
        type: 'line',
        data: {
            labels: labels,
            datasets: [{
                label: label.toString(),
                data: data,
                backgroundColor: [
                    'rgba(255, 255, 132, 0.4)'
                ],
                borderColor: [
                    'rgba(255, 99, 132, 1)',
                ],
                borderWidth: 1
            }]
        },
        options: {
            scales: {
                y: {
                    beginAtZero: true
                }
            }
        }
    });

}