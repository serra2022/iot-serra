"use strict";

const alert = document.getElementById('alert-message');

function showAlert(head, body, green) {
    if(alert == null)
        return;
    green ? alert.classList.add('alert-success') : alert.classList.add('alert-danger');
    alert.innerHTML = `
    <div class="d-flex justify-content-around container-fluid">
        ${green ? '<i class="fa-solid fa-check-circle fa-4x" aria-hidden="true"></i>' : '<i class="fa-solid fa-exclamation-circle fa-4x aria-hidden="true""></i>'}
        <div>
            <h4 class="alert-heading">${head}</h4>
            <p>${body}</p>
        </div>
    </div>`;//<i class="fa-solid fa-triangle-exclamation fa-fade"></i>
    alert.classList.remove('invisible');
    // dismiss the alert after 3 seconds
    setTimeout(() => {
        alert.classList.add('invisible');
        alert.classList.remove('alert-success');
        alert.classList.remove('alert-danger');
        alert.innerHTML = "";
    }, 3000);
};

/**
* makes the element bounce for 3 seconds so that it's easily visible
* @param {*} element the element to be bounced
*/
function makeElementBounce(element) {
   element.classList.add('fa-bounce');
   // stop after 3 seconds
   setTimeout(() => {
       element.classList.remove('fa-bounce');
   }, 3000);
}

export { showAlert, makeElementBounce };