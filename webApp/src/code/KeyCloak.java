package code;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;

public class KeyCloak extends HashMap<String,String> {

	public KeyCloak(String keycloakPath, String params) throws IOException {
		BufferedReader data;
		String json = "";
		String line;
		data = new BufferedReader(new FileReader(keycloakPath));
		while((line = data.readLine())!=null) json += line;
		JsonParser parser = new JsonParser();
		JsonElement element = parser.parse(json);
		if (element.isJsonObject()) {
			JsonObject keycloak = element.getAsJsonObject();
			put("realm", keycloak.get("realm").getAsString());
			put("auth-server-url", keycloak.get("auth-server-url").getAsString());
			put("ssl-required", keycloak.get("ssl-required").getAsString());
			put("resouce", keycloak.get("resource").getAsString());
			put("client-id", keycloak.get("resource").getAsString());
			put("public-client", keycloak.get("public-client").getAsString());
			put("confidential-port", keycloak.get("confidential-port").getAsString());
		}
		data.close();
		BufferedReader data2 = new BufferedReader(new FileReader(params));
		json = "";
		while((line = data2.readLine())!=null) json += line;
		element = parser.parse(json);
		if (element.isJsonObject()) {
			JsonObject parameters = element.getAsJsonObject();
			put("coderq", parameters.get("coderq").getAsString());
			put("tokenrq", parameters.get("tokenrq").getAsString());
			put("extrabodyTemplate", parameters.get("extrabody").getAsString());
			put("redirect-uri", parameters.get("redirect_uri").getAsString());
		}
		data2.close();
	}

	public String getRedirectUri() {
		return get("redirect-uri");
	}

	public String getRealm() {
		return get("realm");
	}

	public String getAuthServer() {
		String val = get("auth-server-url");
		int i = val.length()-2;
		while(i>0 && val.charAt(i)!='/') i--;
		return val.substring(i+1,val.length()-1);
	}

}