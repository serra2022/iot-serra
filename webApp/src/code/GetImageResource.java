package code;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpsExchange;

public class ImageResource implements HttpHandler {

	@Override
	public void handle(HttpExchange ex) throws IOException {
        HttpsExchange exchange = (HttpsExchange) ex;
		String requestURI = exchange.getRequestURI().toASCIIString().replace("/secured/home/", "/");

		String requestMethod = exchange.getRequestMethod();
		if (!Utility.areStringsEquals(requestMethod, "GET")) {
			Utility.notAllowed(exchange);
			return;
		}
		BufferedImage image = getLocalImage(requestURI);
		if (image == null){
			Utility.notFound(exchange);
			return;
		}
		List<String> strlist = new ArrayList<>();
		strlist.add("image/png");
		exchange.getResponseHeaders().put("content-type", strlist);
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		ImageIO.write(image, "png", baos);
		baos.flush();
		byte[] imageInByte = baos.toByteArray();
		baos.close();
		Utility.sendOkResponse(imageInByte, exchange);
	}

	private BufferedImage getLocalImage(String uri) {
		String page = Server.CLIENT_PATH + uri;
		try {
			File f = new File(page);
			return ImageIO.read(f);
		} catch (IOException e) {
			System.out.println("Error reading file '" + page + "'");
			return null;
		}
	}

}
