package code;

import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpsExchange;

public class GetToken implements HttpHandler{

	private final KeyCloak keyCloak;

	public GetToken(KeyCloak keyCloak) {
		this.keyCloak = keyCloak;
	}

	@Override
    public void handle(HttpExchange ex) throws IOException {
        HttpsExchange exchange = (HttpsExchange) ex;
        URI requestURI = exchange.getRequestURI();
        String stringURI = requestURI.toString();
        boolean wantsRedirectPage = Utility.areStringsEquals(stringURI,URI.create("/").toString());
        boolean wantsToken = Utility.areStringsEquals(stringURI,URI.create("/secured").toString());

        if(!wantsRedirectPage && !wantsToken) {
            Utility.notFound(exchange);
            return;
        }

        String requestMethod = exchange.getRequestMethod();
        if (!Utility.areStringsEquals(requestMethod, "GET")) {
            Utility.notAllowed(exchange);
            return;
        }
        // get the html page
        String response = null;
        if(wantsRedirectPage)
        response = Utility.getContent("redirect.html");
        else if(wantsToken)
        response = Utility.getContent("domains.html");

        if(response == null || Utility.areStringsEquals(response, "fail")){
            Utility.serverError(exchange);
            return;
        }
        response = response.replace("$DOMAIN", keyCloak.getAuthServer())
        .replace("$REALM", keyCloak.getRealm())
        .replace("$MY_REDIRECT_URI", keyCloak.getRedirectUri());

        List<String> strlist = new ArrayList<>();
        strlist.add("text/html");
        exchange.getResponseHeaders().put("content-type", strlist);
        Utility.sendOkResponse(response, exchange);
    }

}