package code;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpsExchange;

public class Home implements HttpHandler {

    @Override
    public void handle(HttpExchange ex) throws IOException {
        HttpsExchange exchange = (HttpsExchange) ex;
        String requestMethod = exchange.getRequestMethod();
        if (!Utility.areStringsEquals(requestMethod, "GET")) {
            Utility.notAllowed(exchange);
            return;
        }
        String response = Utility.getContent("domains.html");
        if(response == null || Utility.areStringsEquals(response, "fail")){
            Utility.serverError(exchange);
            return;
        }
        List<String> strlist = new ArrayList<>();
        strlist.add("text/html");
        exchange.getResponseHeaders().put("content-type", strlist);
        Utility.sendOkResponse(response, exchange);
    }

}