package code;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStream;

import com.sun.net.httpserver.HttpsExchange;

public class Utility {

	public static void notFound(HttpsExchange exchange) throws IOException {
		System.out.println("Page not found!");
		String response = "{\"message\":\"Page not found!\"}";
		sendResponse(exchange, response, 404);
	}

	public static void notAllowed(HttpsExchange exchange) throws IOException {
		System.out.println("Method not allowed!");
		String response = "{\"message\":\"Method not allowed!\"}";
		sendResponse(exchange, response, 405);
	}

	public static void serverError(HttpsExchange exchange) throws IOException{
		System.out.println("Server error!");
		String response = "{\"message\":\"Server error!\"}";
		sendResponse(exchange, response, 500);
	}

	public static String getExtension(String file) {
        int i = file.length() - 1;
        while (i > 0 && file.charAt(i) != '.' && file.charAt(i) != '/')
            i--;
        if (file.charAt(i) == '.')
            return file.substring(i + 1);
        else
            return "";
    }

	public static String getContent(String file){
        String line;
        String page = Server.CLIENT_PATH+(file.startsWith("/") ? file : "/"+file);

        StringBuilder answer = new StringBuilder();
        if (Utility.getExtension(page).length() == 0)
            page += ".html";

        BufferedReader bufferedReader = null;
        try {
            FileReader fileReader = new FileReader(page);

            bufferedReader = new BufferedReader(fileReader);
            boolean isComment = false;
            while ((line = bufferedReader.readLine()) != null) {
            	line = line.trim();

            	if(line.startsWith("<!--") && line.endsWith("-->")) {
            		continue;
            	}
            	if(line.startsWith("<!--")) {
            		isComment = true;
            		continue;
            	}
            	if(line.endsWith("-->")) {
            		isComment = false;
            		continue;
            	}

            	if(!isComment && line.length()>0)
                	answer.append(line).append("\n");
            }
        } catch (FileNotFoundException ex) {
            System.out.println("Unable to open file '" + page + "'");
            return "fail";
        } catch (IOException ex) {
            System.out.println("Error reading file '" + page + "'");
            return "fail";
        } finally {
            try{
                if(bufferedReader != null)
                    bufferedReader.close();
            } catch (IOException ex){
                System.out.println("Error closing bufferedReader");
            }
        }
        return answer.toString();
    }

	private static void sendResponse(HttpsExchange exchange, byte[] bytes, int code) throws IOException{
		exchange.sendResponseHeaders(code, bytes.length);
		OutputStream os = exchange.getResponseBody();
		os.write(bytes);
		os.close();
	}

	private static void sendResponse(HttpsExchange exchange, String response, int code) throws IOException {
		sendResponse(exchange, response.getBytes(), code);
	}

	public static void sendOkResponse(byte[] response, HttpsExchange exchange) throws IOException {
		sendResponse(exchange, response, 200);
	}

	public static void sendOkResponse(String response, HttpsExchange exchange) throws IOException {
		sendResponse(exchange, response, 200);
	}

	public static boolean areStringsEquals(String s1, String s2){
		return s1.compareToIgnoreCase(s2) == 0;
	}
}
