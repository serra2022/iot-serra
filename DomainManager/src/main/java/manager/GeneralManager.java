package manager;

import entity.Package;
import entity.*;
import hibernate.HibernateUtils;
import org.hibernate.Session;
import org.json.JSONArray;

import javax.persistence.EntityManager;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class GeneralManager {

    public static EntityManager em;

    public static void initHibernate() {
        em = HibernateUtils.getEntityManager();
    }

    public static void saveRow(Serializable serializable) {
        boolean up = em.getTransaction().isActive();
        if(!up)
            em.getTransaction().begin();
        em.persist(serializable);
        if(!up)
            em.getTransaction().commit();
        System.out.println("fine save");
    }

    public static void updateRow(Serializable serializable) {
        boolean up = em.getTransaction().isActive();
        if(!up)
            em.getTransaction().begin();
        em.merge(serializable);
        if(!up)
            em.getTransaction().commit();
    }

    public static void saveOrUpdate(Serializable serializable) {
        em.getTransaction().begin();
        Session s = em.unwrap(Session.class);
        s.saveOrUpdate(serializable);
        em.getTransaction().commit();
        System.out.println("ok?");
        /*em.getTransaction().begin();
        try {
            CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
            CriteriaQuery<? extends Serializable> criteriaQuery = criteriaBuilder.createQuery(serializable.getClass());
            Root<? extends Serializable> itemRoot = criteriaQuery.from(serializable.getClass());
            List<Field> fieldsIds = Arrays.stream(serializable.getClass().getFields())
                    .filter(f->f.getAnnotation(Id.class)!=null)
                    .collect(Collectors.toList());
            List<Predicate> wherePredicate = new ArrayList<>();
            for(Field field : fieldsIds) {
                if(!field.isAccessible()) {
                    field.setAccessible(true);
                }
                wherePredicate.add(criteriaBuilder.equal(itemRoot.get(field.getName()), field.get(serializable)));
            }
            Predicate finalPredicate = criteriaBuilder.and(wherePredicate.toArray(new Predicate[wherePredicate.size()]));
            criteriaQuery.where(finalPredicate);
            TypedQuery<? extends Serializable> query = em.createQuery(criteriaQuery);

            try {
                query.getSingleResult();
                em.merge(serializable);
            } catch(NoResultException dummy) {
                em.persist(serializable);
            }
            em.getTransaction().commit();
            return true;
        } catch (IllegalArgumentException | IllegalAccessException | SecurityException | NonUniqueResultException e) {
            em.getTransaction().rollback();
            return false;
        }*/
    }

    public static <T>T findEntity(Class<T> c, String str) {
        boolean up = em.getTransaction().isActive();
        if(!up)
            em.getTransaction().begin();
        T t = em.find(c, str);
        if (!up)
            em.getTransaction().commit();
        return t;
    }

    public static <T> void removeEntity(Class<T> c, String str) {
        boolean up = em.getTransaction().isActive();
        if(!up)
            em.getTransaction().begin();
        try {
            T obj = findEntity(c, str);
            System.out.println("prima di remove, dominio c'è? " + em.contains(obj));
            System.out.println("remove "+ obj);
            Session s = em.unwrap(Session.class);
            s.createQuery("Delete FROM Domain D WHERE D.name = :d")
                    .setParameter("d", str).executeUpdate();
            s.createQuery("Delete FROM Service S WHERE S.domain.name = :s")
                    .setParameter("s", str).executeUpdate();
            s.flush();
            s.clear();
            System.out.println("dopo remove, dominio c'è? " + em.contains(obj));
        } catch (Exception e) {
            e.printStackTrace();
        }
        if(!up)
            em.getTransaction().commit();
    }

    public static List<?> findAllRows(Class<?> serializable) {
        return em.createQuery("FROM "+ serializable.getSimpleName(), serializable).getResultList();
    }

    public static JSONArray getServicesInUseByDomain(String domain) {
        Domain dom = findEntity(Domain.class, domain);
        List<Service> services = em.createQuery("FROM Service S WHERE S.domain = :d", Service.class).setParameter("d", dom).getResultList();
        ArrayList<String> arr = new ArrayList<>();// serve per il contains
        JSONArray res = new JSONArray();
        for(Service s : services) {
            String module = s.getPackage().getModule();
            if (!arr.contains(module)) {
                arr.add(module);
                res.put(module);
            }
        }
        return res;
    }

    public static void addRole(String user, String role, String domain) {
        System.out.println("ok add role?");
        User us = findEntity(User.class, user);
        Domain dom = findEntity(Domain.class, domain);
        Role r = new Role(us, dom, role);
        if(GeneralManager.em.find(Role.class, r) != null)
            updateRow(r);
        else
            saveRow(r);
        System.out.println("si add role?");
    }

    public static ArrayList<String> getDomainsUsedOrAdministered(String user, String role) {
        User us = findEntity(User.class, user);
        List<Domain> domains = em.createQuery("Select R.domain FROM Role R WHERE R.user = :u AND R.role = :r", Domain.class)
                .setParameter("u", us).setParameter("r", role).getResultList();
        ArrayList<String> res = new ArrayList<>();
        for (Domain dom: domains) {
            res.add(dom.getName());
        }
        return res;
    }

    public static String getUri(String module) {
        return "https://gitlab.di.unipmn.it/serra2022/iot-serra/-/raw/main/files/" +
                em.createQuery("Select P.file FROM Package P WHERE P.module = :m", String.class)
                .setParameter("m", module).getSingleResult();
    }

    public static List<String> getDomainPackages(String domain) {
        Domain dom = findEntity(Domain.class, domain);
        List<Package> res;
        List<String> packages = new ArrayList<>();
        try {
            res = em.createQuery("Select S.aPackage FROM Service S WHERE S.domain = :d GROUP BY S.aPackage", Package.class)
                    .setParameter("d", dom).getResultList();
            System.out.println("dio cane " + res);
            for(Package p : res){
                packages.add(p.getModule());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return packages;
    }

    public static void updateStatusDomain(String domain, Boolean status) {
        Domain dom = findEntity(Domain.class, domain);
        dom.setStatus(status);
        updateRow(dom);
    }

    public static void addService(String aPackage, String domain, String resource) {
        Package pack = findEntity(Package.class, aPackage);
        Domain dom = findEntity(Domain.class, domain);
        Resource res = findEntity(Resource.class, resource);
        Service s = new Service(dom, res, pack);
        saveRow(s);
    }

    public static ArrayList<String> addService(String domain, String module) {
        System.out.println("dentro add service");
        List<String> reqs = em.createQuery("Select P.requirements FROM Package P WHERE P.module = :m", String.class)
                .setParameter("m", module).getResultList();
        System.out.println("fine add service");
        ArrayList<String> requirements = new ArrayList<>();
        for(String req : reqs) {
            requirements.addAll(Arrays.asList(req.split(",")));
        }
        System.out.println(requirements);
        ArrayList<String> hosts = new ArrayList<>();
        for (String req : requirements) {
            hosts.addAll(em.createQuery("Select R.host FROM Resource R WHERE R.platform = :p", String.class)
                    .setParameter("p", req.trim()).getResultList());
        }
        ArrayList<String> result = new ArrayList<>();
        for (String host : hosts) {
            addService(module, domain, host);
            result.add(module + "---" + host);
        }
        return result;
    }
}