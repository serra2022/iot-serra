package DomainManager;

import com.sun.net.httpserver.HttpsConfigurator;
import com.sun.net.httpserver.HttpsParameters;
import com.sun.net.httpserver.HttpsServer;
import entity.Package;
import entity.Service;
import manager.GeneralManager;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import javax.net.ssl.*;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.URL;
import java.security.KeyStore;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;


public class DomainManager {

	static public int port = 3001;

	public static void main(String[] args) throws IOException, JSONException {
		GeneralManager.initHibernate();
		ThreadPoolExecutor threadPoolExecutor = (ThreadPoolExecutor) Executors.newCachedThreadPool();
		HttpsServer server = HttpsServer.create(new InetSocketAddress(port), 0);// port gets set here

		// initialise the HTTPS server
		try {
			SSLContext sslContext = SSLContext.getInstance("TLS");

			// initialise the keystore
			char[] password = "miapasswd".toCharArray();
			KeyStore ks = KeyStore.getInstance("JKS");
			FileInputStream fis = new FileInputStream("./src/lig.keystore");
			ks.load(fis, password);

			// setup the key manager factory
			KeyManagerFactory kmf = KeyManagerFactory.getInstance("SunX509");
			kmf.init(ks, password);

			// setup the trust manager factory
			TrustManagerFactory tmf = TrustManagerFactory.getInstance("SunX509");
			tmf.init(ks);

			// setup the HTTPS context and parameters
			sslContext.init(kmf.getKeyManagers(), tmf.getTrustManagers(), null);
			server.setHttpsConfigurator(new HttpsConfigurator(sslContext) {
				@Override
				public void configure(HttpsParameters params) {
					try {
						// initialise the SSL context
						SSLContext context = getSSLContext();
						SSLEngine engine = context.createSSLEngine();
						params.setNeedClientAuth(false);
						params.setCipherSuites(engine.getEnabledCipherSuites());
						params.setProtocols(engine.getEnabledProtocols());

						// Set the SSL parameters
						SSLParameters sslParameters = context.getSupportedSSLParameters();
						params.setSSLParameters(sslParameters);

					} catch (Exception ex) {
						System.out.println("Failed to create HTTPS port");
						ex.printStackTrace();
					}
				}
			});

			// chiamata per popolare moduli

			URL url = new URL("https://gitlab.di.unipmn.it/serra2022/iot-serra/-/raw/main/files/index.json");
			HttpsURLConnection con = (HttpsURLConnection) url.openConnection();

			Utility.setConnectionSettings(con, "GET");

			// leggo risposta
			con.getResponseCode();
			// controllare ToDo
			String content = Utility.getResponseFromConnection(con);
			con.disconnect();

			// manipolazione per ottenere i campi dei moduli
			riempiModuli(content);

			server.setExecutor(threadPoolExecutor);

			// API del server

			server.createContext("/install/", new InstallHandler());
			server.createContext("/start/", new StartHandler());
			server.createContext("/stop/", new StopHandler());
			server.createContext("/delete/", new DeleteHandler());
			server.createContext("/secured/domains/", new TokenHandler());
			server.createContext("/secured/services", new ServicesHandler());
			server.createContext("/secured/priviledges", new UserPrivilegesHandler());

			server.start();
			System.out.println("domain running on " + Utility.getDomainURL());

		} catch (Exception e) {
			System.out.println("Failed to create HTTPS server on port " + port + Utility.getDomainURL());
			e.printStackTrace();
		}
	}

	private static void riempiModuli(String content) throws JSONException {
		JSONObject obj = new JSONObject(content);
		JSONArray modules = obj.getJSONArray("response");

		for (int i = 0; i < modules.length(); i++) {
			String remZip = modules.getString(i).substring(0, modules.getString(i).length() - 4);
			String[] current = remZip.split("-");
			String module = current[0];
			try {
				Package p;
				List<Service> services = new ArrayList<>();
				if (current.length > 1) {
					// inserimento modulo non arduino
					String requirements = current[1];
					p = new Package(module, requirements, modules.getString(i), services);
				} else // inserimento modulo arduino
					p = new Package(module, "arduino", modules.getString(i), services);
				Package pp = GeneralManager.findEntity(Package.class, p.getModule());
				if(pp != null)
					GeneralManager.updateRow(p);
				else
					GeneralManager.saveRow(p);
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
	}

}