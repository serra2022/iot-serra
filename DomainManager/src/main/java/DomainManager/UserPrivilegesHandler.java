package DomainManager;


import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;

import manager.GeneralManager;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpsExchange;
import com.sun.net.httpserver.HttpHandler;


public class UserPrivilegesHandler implements HttpHandler{

    public void handle(HttpExchange hex) throws IOException {
        HttpsExchange he = (HttpsExchange) hex;
        // String response = "{\"priviledges\":\"admin\"}" || "{\"priviledges\":\"user\"}";
        // check if the call is an options
        String requestMethod = he.getRequestMethod();
        if(requestMethod.compareToIgnoreCase("options") == 0) {
            Utility.sendCors(he, 200);
            return;
        }
        // exclude any request that isn't a get
        if(requestMethod.compareToIgnoreCase("get") != 0) {
            Utility.sendCors(he, 405);
            return;
        }
        // check if the user is logged in
        String user;
        if((user = Utility.checkTokenGetUser(he)) == null) {
            Utility.sendCors(he, 401);
            return;
        }
        // get the query
        String query = he.getRequestURI().getRawQuery();
        if(query == null) {
            Utility.sendCors(he, 400);
            return;
        }
        // parse the query
        Map<String, Object> parameters = Utility.parseQuery(query);
        // get the domain for which we want to know the priviledges
        String domain = (String) parameters.get("domain");
        // query the database
        ArrayList<String> adminlist = GeneralManager.getDomainsUsedOrAdministered(user, "A");
        if(!adminlist.isEmpty() && adminlist.contains(domain)) {
            // the user is admin of this domain
            Utility.sendCors(he, 200, "{\"priviledges\":\"admin\"}");
            return;
        }
        ArrayList<String> userlist = GeneralManager.getDomainsUsedOrAdministered(user, "U");
        if(!userlist.isEmpty() && userlist.contains(domain)) {
            // the user is user of this domain
            Utility.sendCors(he, 200, "{\"priviledges\":\"user\"}");
            return;
        }
        // the user is not admin or user of this domain
        Utility.sendCors(he, 404);
    }
}