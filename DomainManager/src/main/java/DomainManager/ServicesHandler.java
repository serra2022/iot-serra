package DomainManager;


import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import entity.Package;
import manager.GeneralManager;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpsExchange;
import com.sun.net.httpserver.HttpHandler;

public class ServicesHandler implements HttpHandler {

    public void handle(HttpExchange hex) throws IOException {
        HttpsExchange he = (HttpsExchange) hex;
        URI requestedUri = he.getRequestURI();
        String requestMethod = he.getRequestMethod();

        if (requestMethod.compareToIgnoreCase("options") == 0) {
            Utility.sendCors(he, 200);
            return;
        }
        if (requestMethod.compareToIgnoreCase("GET") != 0) {
            Utility.sendCors(he, 405);
            return;
        }
        if(Utility.checkTokenGetUser(he) == null) {
            Utility.sendCors(he, 401);
            return;
        }
        String response = "";
        // get the query (if any)
        String query = requestedUri.getRawQuery();
        String domain = null;
        if(query != null){
            Map<String, Object> parameters = Utility.parseQuery(requestedUri.getRawQuery());
            domain=(String) parameters.get("domain");
        }
        JSONObject res = new JSONObject();
        if(domain != null){
            // I have to send only the services used by this domain
            JSONArray rs =null;
            try {
                rs= GeneralManager.getServicesInUseByDomain(domain);
                res.put("response", rs);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
            // I have to send all available services
            ArrayList<String> arr = new ArrayList<String>();
            JSONArray resu = new JSONArray();
            List<Package> packages = (List<Package>) GeneralManager.findAllRows(Package.class);
            for (Package p : packages) {
                String resString = p.getModule();
                if (!arr.contains(resString)) {
                    arr.add(resString);
                    resu.put(resString);
                }
            }
            try {
                res.put("response",resu);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        response = res.toString();
        // questa parte sopra serve anche qui, non solo quando si chiama con OPTIONS
        Utility.sendCors(he, 200, response);
    }
}