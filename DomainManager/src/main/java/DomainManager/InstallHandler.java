package DomainManager;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import entity.Domain;
import entity.Service;
import entity.User;
import manager.GeneralManager;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class InstallHandler implements HttpHandler {

	@Override
	public void handle(HttpExchange he) throws IOException {
		// HttpsExchange he = (HttpsExchange) hex;
		String requestMethod = he.getRequestMethod();
		System.out.println(requestMethod);
		if (requestMethod.compareToIgnoreCase("options") == 0) {
			Utility.sendCors(he, 200);
			return;
		}
		if (requestMethod.compareToIgnoreCase("POST") != 0) {
			Utility.sendCors(he, 405);
			return;
		}
		String body = Utility.readBody(he.getRequestBody());
		// verifica user
		String user = Utility.checkTokenGetUser(he);
		System.out.println(user);
		if (user == null) {
			Utility.sendCors(he, 401);
			return;
		}
		JSONObject j;
		System.out.println("bbbbbbbbbbbbbb");
		try {
			j = new JSONObject(body);
			String dm = j.getString("domain");
			System.out.println(dm);
			Domain d = GeneralManager.findEntity(Domain.class, dm);
			System.out.print("esiste: " + d + "? ");
			if (d != null) {
				System.out.println(" si");
				Utility.sendCors(he, 403);
				return;
			}
		} catch (JSONException e) {
			e.printStackTrace();
			return;
		}

		// inserisco i dati nel DB
		try {
			// qui leggo e parsifico i json nel body, inserisco tutti i campi nel db
			System.out.println("no");
			String domain = j.getString("domain");
			List<Service> services = new ArrayList<>();
			Domain d = new Domain(domain, "", false, services);
			GeneralManager.saveRow(d);
			System.out.println("ora esiste");
			// questa chiamata serve ad assicurarsi che l'utente che ha effettuato la
			// chiamata sia anche amministratore del dominio
			GeneralManager.addRole(user, "A", domain);
			JSONArray arrUsers = j.getJSONArray("users");
			System.out.println(arrUsers);
			for (int i = 0; i < arrUsers.length(); i++) {
				JSONObject userObj = arrUsers.getJSONObject(i);
				String usr = userObj.getString("user");
				String passwd = userObj.getString("passwd");
				if (user.equals(usr))
					continue;
				if (userObj.getString("role").equals("A")) {
					System.out.println(userObj);
					User u = GeneralManager.findEntity(User.class, usr);
					if(u != null)
						GeneralManager.updateRow(new User(usr, "", passwd));
					else
						GeneralManager.saveRow(new User(usr, "", passwd));
					GeneralManager.addRole(usr, "A", domain);
				} else if (userObj.getString("role").equals("U")) {
					System.out.println(userObj);
					User u = GeneralManager.findEntity(User.class, usr);
					if(u != null)
						GeneralManager.updateRow(new User(usr, "", passwd));
					else
						GeneralManager.saveRow(new User(usr, "", passwd));
					GeneralManager.addRole(usr, "U", domain);
				} else
					System.err.println(userObj);
			}
			System.out.println("frocio");
			JSONArray arrServ = j.getJSONArray("services");// array con solo il nome dei servizi da installare
			System.out.println("?"+arrServ);
			ArrayList<String> moduleHosts = new ArrayList<>();
			for (int i = 0; i < arrServ.length(); i++) {
				String modul = arrServ.getString(i);
				moduleHosts.addAll(GeneralManager.addService(domain, modul));
			}
			System.out.println("moduleHosts "+ moduleHosts);
			// divido gli host in base al loro module
			HashMap<String, ArrayList<String>> hostsMap = new HashMap<>();
			for (String mh : moduleHosts) {
				String[] mhSplit = mh.split("---");
				String module = mhSplit[0];
				String host = mhSplit[1];
				ArrayList<String> hostsList;
				if (hostsMap.containsKey(module)) {
					hostsList = hostsMap.get(module);
				} else {
					hostsList = new ArrayList<>();
					hostsMap.put(module, hostsList);
				}
				hostsList.add(host);
			}
			// imposta correttamente l'array dei servizi
			JSONArray arrServiziFinale = new JSONArray();
			for (int i = 0; i < arrServ.length(); i++) {
				JSONObject row = new JSONObject();
				String mod = arrServ.getString(i);
				ArrayList<String> ho = hostsMap.get(mod);
				for (String h : ho) {
					row.put("host", "serra");
					row.put("service", mod);
					row.put("uri", GeneralManager.getUri(mod));
					arrServiziFinale.put(row);
				}
			}
			j.put("services", arrServiziFinale);

		} catch (Exception e) {
			e.printStackTrace();
		}
		// ora bisogna fare la chiamata al CloudApp, non prima!
		// effettuo chiamata a CloudAppManager

		// è una chiamata annidata nella risposta alla webapp
		// -richiesta REST da webApp a /install
		// -prendo da DB e poi chiamo CloudAppMng su /install
		// -attendo risposta da CloudAppMng e chiudo
		// -rispondo a webApp e chiudo
		// EZ
		System.out.println("lvi? " + j);
		HttpURLConnection con = Utility.sendMessageToCloudApp("install", j.toString());
		System.out.println(con);
		int status = con.getResponseCode();
		// FIXME serve avere anche il content?
		// String cloudappResponse = Helper.getResponseFromConnection(con);
		con.disconnect();

		// finita chiamata a CloudApp
		Utility.sendCors(he, status);
		GeneralManager.em.flush();
		GeneralManager.em.clear();
	}
}