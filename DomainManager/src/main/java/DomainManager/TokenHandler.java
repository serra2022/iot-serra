package DomainManager;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpsExchange;
import entity.Domain;
import io.fusionauth.jwt.InvalidJWTSignatureException;
import io.fusionauth.jwt.Verifier;
import io.fusionauth.jwt.domain.Algorithm;
import io.fusionauth.jwt.domain.JWT;
import io.fusionauth.jwt.rsa.RSAVerifier;
import manager.GeneralManager;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

public class TokenHandler implements HttpHandler {

    public void handle(HttpExchange hex) throws IOException {
        HttpsExchange he = (HttpsExchange) hex;
        String requestMethod = he.getRequestMethod();

        if (requestMethod.compareToIgnoreCase("options") == 0) {
            Utility.sendCors(he, 200);
            return;
        }
        if (requestMethod.compareToIgnoreCase("GET") != 0) {
            Utility.sendCors(he, 405);
            return;
        }

        String user = Utility.checkTokenGetUser(he);
        if (user == null) {
            Utility.sendCors(he, 401);
            return;
        }
        // ricavo da token

        JSONObject res = new JSONObject();
        JSONArray rs = new JSONArray();

        ArrayList<String> domList;
        try {
            domList = GeneralManager.getDomainsUsedOrAdministered(user, "U");
            for (String dom : domList) {
                JSONObject ogg = new JSONObject();
                ogg.put("nome", dom);

                List<String> packages = GeneralManager.getDomainPackages(dom);
                int tipo;
                if (packages.size() == 3)
                    tipo = 0;
                else if (packages.size() == 2)
                    if(packages.contains("archiver"))
                        tipo = 1;
                    else
                        tipo = 2;
                else
                    if (packages.contains("water"))
                        tipo = 3;
                    else
                        tipo = 4;

                ogg.put("tipo", tipo);

                Domain d = (Domain) GeneralManager.findEntity(Domain.class, dom);
                ogg.put("stato", d.getStatus());
                ogg.put("admin", false);

                rs.put(ogg);
            }
            domList.clear();
            domList = GeneralManager.getDomainsUsedOrAdministered(user, "A");
            for (String dom : domList) {
                JSONObject ogg = new JSONObject();
                ogg.put("nome", dom);// domain
                List<String> packages = GeneralManager.getDomainPackages(dom);
                System.out.println("size " + packages.size());
                int tipo;
                if (packages.size() == 3)
                    tipo = 0;
                else if (packages.size() == 2) {
                    if (packages.contains("archiver"))
                        tipo = 1;
                    else
                        tipo = 2;
                } else {
                    if (packages.contains("water"))
                        tipo = 3;
                    else
                        tipo = 4;
                }

                ogg.put("tipo", tipo);
                Domain d = (Domain) GeneralManager.findEntity(Domain.class, dom);
                ogg.put("stato", d.getStatus());
                ogg.put("admin", true);

                rs.put(ogg);
            }
            res.put("response", rs);
            System.out.println("rs " + rs);
        } catch (JSONException e) {
            e.printStackTrace();
            return;
        }

        String response = res.toString();
        Utility.sendCors(he, 200, response);
    }

    public static String verificaToken(String encodedJWT, String signature)
            throws IOException, JSONException, NoSuchAlgorithmException {

        URL url = new URL(Utility.getKeycloakURL() + "realms/" + Utility.getKeycloakRealm() + "/protocol/openid-connect/certs");// maybe, 8080
        HttpURLConnection con = (HttpURLConnection) url.openConnection();
        setConnectionSettings(con, "GET");

        // leggo risposta
        int status = con.getResponseCode();

        String content = getResponseFromConnection(con);
        con.disconnect();

        JSONObject j = new JSONObject(content);
        JSONArray arr = j.getJSONArray("keys");
        JSONObject ogg = arr.getJSONObject(0);
        String chiave = ogg.getJSONArray("x5c").get(0).toString();
        String cert = "-----BEGIN CERTIFICATE-----\n" + chiave + "\n-----END CERTIFICATE-----";

        Verifier verifier = RSAVerifier.newVerifier(cert);
        try {
            int index = encodedJWT.lastIndexOf('.');
            byte[] message = encodedJWT.substring(0, index).getBytes(StandardCharsets.UTF_8);
            byte[] signatureBytes = Base64.getUrlDecoder().decode(signature);// signature = tokSplit[2]
            verifier.verify(Algorithm.RS256, message, signatureBytes);
        } catch (InvalidJWTSignatureException e) {
            return null;
        }
        // Verify and decode the encoded string JWT to a rich object
        JWT jwt = JWT.getDecoder().decode(encodedJWT, verifier);
        return jwt.getString("preferred_username");
    }

    private static void setConnectionSettings(HttpURLConnection connection, String method) throws IOException{
        connection.setRequestMethod(method);
        connection.setRequestProperty("Content-Type", "application/json");
        connection.setRequestProperty("Accept", "application/json");
        connection.setDoOutput(true);
        connection.setConnectTimeout(5000);
        connection.setReadTimeout(5000);
    }

    private static String getResponseFromConnection(HttpURLConnection con) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuilder content = new StringBuilder();
        while ((inputLine = reader.readLine()) != null) {
            content.append(inputLine);
        }
        reader.close();
        return content.toString();
    }
}