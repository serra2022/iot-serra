package DomainManager;

import java.io.IOException;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

import java.util.ArrayList;

import manager.GeneralManager;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.HttpURLConnection;

public class StopHandler implements HttpHandler {

	@Override
	public void handle(HttpExchange he) throws IOException {
		// HttpsExchange he = (HttpsExchange) hex;
		String requestMethod = he.getRequestMethod();

		if (requestMethod.compareToIgnoreCase("options") == 0) {
			Utility.sendCors(he, 200);
			return;
		}
		if (requestMethod.compareToIgnoreCase("POST") != 0) {
			Utility.sendCors(he, 405);
			return;
		}

		String body = Utility.readBody(he.getRequestBody());
		String user= Utility.checkTokenGetUser(he);
		if (user == null) {
			Utility.sendCors(he, 401);
			return;
		}
		String domain;
		try {
			domain = new JSONObject(body).getString("domain");

			ArrayList<String> ad = GeneralManager.getDomainsUsedOrAdministered(user, "A");
			if (!ad.contains(domain)) {
				Utility.sendCors(he, 401);
				return;
			}

		} catch (JSONException e) {
			e.printStackTrace();
			return;
		}

		HttpURLConnection con = Utility.sendMessageToCloudApp("stop", body);
		int status = con.getResponseCode();
		// FIXME serve avere anche il content?
		// String cloudappResponse = Helper.getResponseFromConnection(con);
		con.disconnect();

		Utility.sendCors(he, status);
		GeneralManager.updateStatusDomain(domain, false);
	}
}