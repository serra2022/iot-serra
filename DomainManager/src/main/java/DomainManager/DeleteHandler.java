package DomainManager;

import java.io.IOException;
import java.util.ArrayList;

import entity.Domain;
import manager.GeneralManager;
import org.json.JSONObject;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import java.net.HttpURLConnection;

public class DeleteHandler implements HttpHandler {

	@Override
	public void handle(HttpExchange he) throws IOException {
		// HttpsExchange he = (HttpsExchange) hex;
		String requestMethod = he.getRequestMethod();

		if (requestMethod.compareToIgnoreCase("options") == 0) {
			Utility.sendCors(he, 200);
			return;
		}
		if (requestMethod.compareToIgnoreCase("POST") != 0) {
			Utility.sendCors(he, 405);
			return;
		}

		String body = Utility.readBody(he.getRequestBody());
		String user= Utility.checkTokenGetUser(he);
		if (user == null) {
			System.out.println("null user");
			Utility.sendCors(he, 401);
			return;
		}
		try {
			String dominio = new JSONObject(body).getString("domain");

			Domain d = (Domain) GeneralManager.findEntity(Domain.class, dominio);
			System.out.println(d);
			if (d == null) {
				System.out.println("null domain");
				Utility.sendCors(he, 404);
				return;
			}
			ArrayList<String> ad = GeneralManager.getDomainsUsedOrAdministered(user, "A");
			System.out.println("pippo1 " + ad);
			if (!ad.contains(dominio)) {
				System.out.println("entrato");
				Utility.sendCors(he, 401);
				return;
			}
			GeneralManager.removeEntity(Domain.class, dominio);
		} catch (Exception e) {
			e.printStackTrace();
			return;
		}
		HttpURLConnection con = Utility.sendMessageToCloudApp("delete", body);
		int status = con.getResponseCode();
		System.out.println("pippostatus " + status);
		// FIXME serve avere anche il content?
		// String cloudappResponse = Helper.getResponseFromConnection(con);
		con.disconnect();
		Utility.sendCors(he, status);
	}
}