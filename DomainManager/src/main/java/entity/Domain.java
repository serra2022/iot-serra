package entity;

import javax.persistence.*;


import java.io.Serializable;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "domains")
public class Domain implements Serializable {

    @Id
    @Column(name = "name", length = 64)
    private String name;

    @Column(name = "description", length = 64)
    private String description;

    @Column(name = "status")
    private Boolean status;

    @OneToMany(mappedBy = "domain", orphanRemoval = true)
    private List<Service> services;

    @OneToMany(mappedBy = "domain", cascade = CascadeType.ALL)
    private Set<Role> roles = new HashSet<>();

    @OneToMany(mappedBy = "domain")
    private List<Resource> resources;

    public Domain() {}

    public Domain(String name, String description, Boolean status, List<Service> services) {
        this.name = name;
        this.description = description;
        this.status = status;
        this.services = services;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public Boolean getStatus() {
        return status;
    }

    @Override
    public String toString() {
        return "Domain {name = " + name + ", description = " + description + ", status = " + status +'}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Domain domain = (Domain) o;
        return Objects.equals(name, domain.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }
}