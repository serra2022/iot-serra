package entity;

import javax.persistence.*;
import org.hibernate.annotations.Check;

import java.io.Serializable;
import java.util.Objects;

@Entity
@Table(name = "roles")
public class Role implements Serializable {

    @Id
    @ManyToOne
    @MapsId("user")
    @JoinColumn(name = "user", columnDefinition = "varchar(64)")
    public User user;

    @Id
    @ManyToOne
    @MapsId("domain")
    @JoinColumn(name = "domain", columnDefinition = "varchar(64)")
    public Domain domain;

    @Check(constraints = "DEFAULT 'U' CHECK(\"role\" = 'U' OR \"role\" = 'A')")
    @Column(name = "role", nullable = false, length = 1)
    public String role;

    public Role() {}

    public Role(User user, Domain domain, String role) {
        //this.ids = ids;
        this.user = user;
        this.domain = domain;
        this.role = role;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Domain getDomain() {
        return domain;
    }

    public void setDomain(Domain domain) {
        this.domain = domain;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    @Override
    public String toString() {
        return "Role{user= " + user + ", domain= " + domain + ", role= " + role + '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Role role = (Role) o;
        return Objects.equals(user, role.user) && Objects.equals(domain, role.domain);
    }

    @Override
    public int hashCode() {
        return Objects.hash(user, domain);
    }
}