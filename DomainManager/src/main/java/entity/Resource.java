package entity;

import org.hibernate.annotations.Cascade;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "resources")
public class Resource implements Serializable {

    @Id
    @Column(name = "host", length = 64)
    private String host;

    @Column(name = "IoTpwd", length = 64)
    private String password;

    @Column(name = "platform", length = 64)
    private String platform;

    @OneToMany(mappedBy = "resource")
    private List<Service> services;

    @ManyToOne
    @Cascade(org.hibernate.annotations.CascadeType.ALL)
    @JoinColumn(name = "domain")
    private Domain domain;

    public Resource() {}

    public Resource(String host, String password, String platform, List<Service> services, Domain domain) {
        this.host = host;
        this.password = password;
        this.platform = platform;
        this.services = services;
        this.domain = domain;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPlatform() {
        return platform;
    }

    public void setPlatform(String platform) {
        this.platform = platform;
    }

    public List<Service> getServices() {
        return services;
    }

    public void setServices(List<Service> services) {
        this.services = services;
    }

    public Domain getDomain() {
        return domain;
    }

    public void setDomain(Domain domain) {
        this.domain = domain;
    }

    @Override
    public String toString() {
        return "Resource{host= " + host + ", password= " + password + ", platform= " + platform + ", service= " + services.toString() + ", domain" + domain.toString() + '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Resource resource = (Resource) o;
        return Objects.equals(host, resource.host);
    }

    @Override
    public int hashCode() {
        return Objects.hash(host);
    }
}