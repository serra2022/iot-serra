package entity;

import javax.persistence.*;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "packages")
public class Package implements Serializable {

    @Id
    @Column(name = "module", length = 64)
    private String module;

    @Column(name = "requirements", length = 64)
    private String requirements;

    @Column(name = "file", length = 128)
    private String file;

    @OneToMany(mappedBy = "aPackage")
    private List<Service> services;

    public Package() {}

    public Package(String module, String requirements, String file, List<Service> services) {
        this.module = module;
        this.requirements = requirements;
        this.file = file;
        this.services = services;
    }

    public String getModule() {
        return module;
    }

    public void setModule(String name) {
        this.module = name;
    }

    public String getRequirements() {
        return requirements;
    }

    public void setRequirements(String requirements) {
        this.requirements = requirements;
    }

    public String getFile() {
        return file;
    }

    public void setFile(String file) {
        this.file = file;
    }

    @Override
    public String toString() {
        return "Package{module= " + module + ", requirements= " + requirements + ", file= " + file + '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Package aPackage = (Package) o;
        return Objects.equals(module, aPackage.module);
    }

    @Override
    public int hashCode() {
        return Objects.hash(module);
    }
}