import com.sun.net.httpserver.HttpServer;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.concurrent.Executors;

public class RESTHttpServer {

    static public int port=3002;
    static public Conf conf;
    static public Publisher mqtt_client;

    public static void main(String[] args) throws IOException {
        conf = new Conf("CONF/conf.xml");
        port = Integer.parseInt(conf.get("port"));
        //mqtt_client = new Publisher(conf.protocol+"://"+conf.broker+":"+conf.get("portMqtt"));
        mqtt_client = new Publisher("");
        mqtt_client.start();
        HttpServer server = HttpServer.create(new InetSocketAddress(port),0);


        //API del server
        server.createContext("/apirest", new HtmlPage(conf.confdir));
        server.createContext("/install", new Install(mqtt_client));
        server.createContext("/start", new Start(mqtt_client));
        server.createContext("/stop", new Stop(mqtt_client));
        server.createContext("/delete", new Delete(mqtt_client));
        // ThreadPoolExecutor threadPoolExecutor = (ThreadPoolExecutor)Executors.newFixedThreadPool(5);
        server.setExecutor(Executors.newCachedThreadPool());
        server.start();
        System.out.println("cloudapp running on http://localhost:"+port);
    }
}
